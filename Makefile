PY=python
PELICAN?=pelican
PELICANOPTS?=-D

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
PUBLISHDIR=$(BASEDIR)/public
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

OPENCC_TR=/usr/share/opencc/t2s.json

help:
	@echo 'Makefile for a pelican Web site                                               '
	@echo '                                                                              '
	@echo 'Usage:                                                                        '
	@echo '   make cc                  (re)generate the Simp. Chinese docs               '
	@echo '   make html                (re)generate the web site (incl. cc)              '
	@echo '   make clean               remove the generated files                        '
	@echo '   make cleancc             remove generated Simp. Chinese files              '
	@echo '   make regenerate          regenerate files upon modification (incl. cc)     '
	@echo '   make publish             generate using production settings (incl. cc)     '
	@echo '   make serve               serve site at http://localhost:8000               '
	@echo '   make devserve            serve site and watch changes                      '
	@echo '                                                                              '


serve:
	cd $(OUTPUTDIR) && $(PY) -m pelican.server

devserve:
	pelican -lr


ZH_RST=$(shell find content -iname "*.zh.rst")
ZH_MD=$(shell find content -iname "*.zh.md")

%.zhs.rst: %.zh.rst
	opencc -c $(OPENCC_TR) -i $^ -o $@
	sed -i 's/:lang: zh/:lang: zhs/g' $@
	sed -i 's/\.zh\./\.zhs\./g' $@

%.zhs.md: %.zh.md
	opencc -c $(OPENCC_TR) -i $^ -o $@
	sed -i 's/Lang: zh/Lang: zhs/g' $@
	sed -i 's/\.zh\./\.zhs\./g' $@

cc: $(patsubst %.zh.rst,%.zhs.rst,$(ZH_RST)) $(patsubst %.zh.md,%.zhs.md,$(ZH_MD))


html: cc
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	[ ! -d $(OUTPUTDIR) ] || find $(OUTPUTDIR) -not -type d -not -wholename "*/.git*"  -not -iname "*.pdf" -not -iname "*.png" -delete

cleancc:
	find -iname "*.zhs.rst" -delete ;
	find -iname "*.zhs.md" -delete ;

regenerate: cleancc clean
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

publish: cc
	[ ! -d $(PUBLISHDIR) ] || (cd $(PUBLISHDIR) && find -not -type d -not -wholename "*/.git*"  -not -iname "*.pdf" -not -iname "*.png" -delete)
	$(PELICAN) $(INPUTDIR) -o $(PUBLISHDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

.PHONY: html help clean regenerate publish cc cleancc
