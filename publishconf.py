#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://blog.ryey.icu'
RELATIVE_URLS = False

OUTPUT_PATH = 'public/'

FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ATOM = 'feeds/atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
TAG_FEED_ATOM = 'feeds/tag-{slug}.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

DISQUS_SITENAME = "renyuneyun-gitlab-io"
HYPOTHESIS_GROUP_ID = "vKzYmGWG"

PIWIK_URL = 'tracking.ryey.icu'
PIWIK_SITE_ID = '1'

FLATTR_ID = 'knd0xz'
