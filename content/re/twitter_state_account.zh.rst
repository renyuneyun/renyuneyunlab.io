Twitter上的state affliated標記
#####################################

:date: 2020-12-30 21:30
:modified: 2021-01-4 12:30
:lang: zh
:category: re
:tags: Twitter, 媒體, 陰
:summary: 標記標準不一致


人和組織代表（人）官號
=========================

.. figure:: {static}/images/twitter-state-account/zlj517.png
   :alt: zlj517
   :width: 600

   趙立堅（個人名稱賬號）：時任中華人民共和國外交部副司長、發言人

.. figure:: {static}/images/twitter-state-account/HuXijin_GT.png
   :alt: HuXijin_GT
   :width: 600

   胡錫進（個人名稱賬號）：時任環球時報主編

.. figure:: {static}/images/twitter-state-account/SpokespersonCHN.png
   :alt: SpokespersonSHN
   :width: 600

   中華人民共和國外交部發言人（官方賬號）

.. figure:: {static}/images/twitter-state-account/realDonaldTrump.png
   :alt: realDonaldTrump
   :width: 600

   唐納德·特朗普（個人名稱賬號）：時任美利堅合衆國總統（2016年當選）

.. figure:: {static}/images/twitter-state-account/JoeBiden.png
   :alt: JoeBiden
   :width: 600

   喬·拜登（個人名稱賬號）：或將爲下任（2021年）美利堅合衆國總統

.. figure:: {static}/images/twitter-state-account/POTUS.png
   :alt: POTUS
   :width: 600

   美國總統（官方賬號）

.. figure:: {static}/images/twitter-state-account/BorisJohnson.png
   :alt: BorisJohnson
   :width: 600

   鮑里斯·約翰遜：時任大不列顛及北愛爾蘭聯合王國首相

.. figure:: {static}/images/twitter-state-account/EmmanuelMacron.png
   :alt: EmmanuelMacron
   :width: 600

   埃馬紐埃爾·馬克龍：時任法蘭西共和國總統


組織賬號
========

.. figure:: {static}/images/twitter-state-account/globaltimesnews.png
   :alt: globaltimesnews
   :width: 600

   環球時報

.. figure:: {static}/images/twitter-state-account/RT.png
   :alt: RT
   :width: 600

   Russia Today（今日俄羅斯）

.. figure:: {static}/images/twitter-state-account/BBCNews.png
   :alt: BBCNews
   :width: 600

   BBC（英國廣播公司）

.. figure:: {static}/images/twitter-state-account/NHSuk.png
   :alt: NHSuk
   :width: 600

   NHS（英國國民衛生系統）

.. figure:: {static}/images/twitter-state-account/RoyalNavy.png
   :alt: RoyalNavy
   :width: 600

   英國皇家海軍


其他背景信息
===============

Twitter的state affliated賬號政策
-----------------------------------

.. figure:: {static}/images/twitter-state-account/twitter-state-affliated-1.png
   :target: https://help.twitter.com/en/rules-and-policies/state-affiliated
   :alt: twitter state affliated policy, part 1
   :width: 600

   第一部分

.. figure:: {static}/images/twitter-state-account/twitter-state-affliated-1-china.png
   :target: https://help.twitter.com/en/rules-and-policies/state-affiliated-china
   :alt: twitter state affliated policy, part 1, for China
   :width: 600

   第一部分，中國特供版

.. figure:: {static}/images/twitter-state-account/twitter-state-affliated-2.png
   :alt: twitter state affliated policy, part 2
   :width: 600

   第二部分

BBC的信息
-----------

.. figure:: {static}/images/twitter-state-account/BBC_1.png
   :target: https://www.bbc.co.uk/aboutthebbc
   :alt: BBC information, part 1
   :width: 600

   BBC信息

.. figure:: {static}/images/twitter-state-account/BBC_2.png
   :target: https://www.bbc.co.uk/aboutthebbc/governance/charter
   :alt: BBC information, part 2
   :width: 600

   關於該Royal Charter

RT的信息
-------------

.. figure:: {static}/images/twitter-state-account/RT-info.png
   :target: https://www.rt.com/about-us/
   :alt: RT info
   :width: 600

   RT（今日俄羅斯）的信息


中華人民共和國外交部新聞司信息
------------------------------------

.. figure:: {static}/images/twitter-state-account/wjb-xws.png
   :target: https://www.fmprc.gov.cn/web/wjb_673085/zzjg_673183/xws_674681/
   :alt: 外交部新聞司概況
   :width: 600


其他
--------

1. 未見中華人民共和國主席與俄羅斯聯邦總統在Twitter上的賬號
2. 未見其他國家特供版本的「國家相關賬號（state affliated）」政策

