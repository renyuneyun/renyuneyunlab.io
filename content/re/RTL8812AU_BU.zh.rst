Realtek RTL 8812AU和8812BU
#################################

:date: 2020-06-02 20:00
:lang: zh
:category: re
:tags: 硬件, Realtek, USB, Wireless Card


RTL 8812AU和8812BU均是支持IEEE 802.11ac的USB 3.0無線網卡芯片。

產品介紹及差別
=================

* RTL 8812AU：https://www.realtek.com/en/products/communications-network-ics/item/rtl8812au
* RTL 8812BU：https://www.realtek.com/en/products/communications-network-ics/item/rtl8812bu

功能上，相比RTL 8812AU，RTL 8812BU額外支持Multi-user MIMO (Multiple-Input, Multiple-Output)。

二者封裝（？）也不同：8812AU是QFN-76 package，而8812BU是TFBGA 6.5x6.5mm package。


Linux下開源驅動
=================

* RTL 8812AU：https://github.com/gnab/rtl8812au
* RTL 8812BU：https://github.com/cilynx/rtl88x2bu

