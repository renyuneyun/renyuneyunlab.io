Title: ChatGPT關於區塊鏈上進行訪問控制的回答
Date: 2023-03-18 17:05
Lang: zh
Category: re
Tags: chatGPT, New Bing, GPT-3, GPT-4, blockchain, access control
Slug: chatgpt-blockchain-access-control

和[《論Web3.0和Web3》](on-web-3-0-and-web-3)相關，向chatGPT提關於區塊鏈進行訪問控制的問題，尤其是關於如何限制「讀取」，它的回答。

> 我不是Plus用戶，目前使用的應當還是3月版本的基於GPT-3的chatGPT。
> 
> 我也用了New Bing，但它主要給我甩了一些不對題的文章（都是關於「如何使用smart contract實現access control檢查（而不包含數據使用部分）」），還不如chatGPT。

基本是在複述區塊鏈宣傳，沒有「意識到」自己的前後矛盾之處。典型的語言模型生成結果。



![]({static}/images/chatGPT/blockchain-access-control-read.png)
