Title: 比較Linux軟件箱系統
Date: 2020-06-11 11:51
Lang: zh
Category: re
Tags: Linux, software bundle, FreeDesktop, desktop, 包管理


內容來自於[AppImage的wiki頁](https://github.com/AppImage/AppImageKit/wiki/Similar-projects#comparison)。僅截取其中個人最看重的部分；譯爲中文；進行少量調整；進行少量額外註釋。

基礎
------

| **特性**                                 | **AppImage**                             | **Snap**                                                                                                                                 | **Flatpak**                                                                                                                                                                                                               |
| ---------------------------------------- | ---------------------------------------- | ----------------------------------------                                                                                                 | ----------------------------------------                                                                                                                                                                                  |
| 桌面GUI程序                              | ✅ 支持                                  | ✅  支持                                                                                                                                 | ✅  支持                                                                                                                                                                                                                  |
| 命令行CLI工具                            | ✅  支持                                 | ✅  支持                                                                                                                                 | ✅  支持 (with App ID aliases if you edit PATH)[[1](https://github.com/flatpak/flatpak/releases/tag/0.10.2)]                                                                                                              |
| 服務器進程                               | ✅  支持                                 | ✅  支持                                                                                                                                 | ⚠️ 可行，但非主要目的 [[1](https://flatpak.org/faq/#Can_Flatpak_be_used_on_servers_too_)]                                                                                                                                  |
| 正確應用主題                             | ✅  支持 (需要正確實現)                  | :x: 不支持 (snapd不支持完整的主題整合) [[1](https://youtu.be/gJVKypMwJYI?t=5m48s)] [[2](https://elementaryos.stackexchange.com/a/13728)] | ✅  支持 (若當前系統主題已被Flatpak化/包含Flatpak支持) [[1](https://youtu.be/gJVKypMwJYI?t=6m20s)] [[2](http://www.omgubuntu.co.uk/2017/05/flatpak-theme-issue-fix)] [[3](https://github.com/flatpak/flatpak/issues/114)] |
| 庫/依賴                                  | 基系統或捆綁在appimage內                 | 基發行版snap或通過[部件/插件](https://docs.snapcraft.io/build-snaps/parts)                                                           | Freedesktop, GNOME, KDE 主[運行時](http://docs.flatpak.org/en/latest/available-runtimes.html)或捆綁在內                                                                                                                   |
| 企業背景/支持                            | 社區項目，無企業背景，但被許多企業使用   | Canonical（Ubuntu的東家）                                                                                                                | 多個公司：Endless, Red Hat, Codethink, Igalia                                                                                                                                                                             |

沙箱/限制
-----------------

| **特性**                                 | **AppImage**                                                                                                                                                                                                                                                                                             | **Snap**                                                                                                                                                     | **Flatpak**                                                                              |
| ---------------------------------------- | ----------------------------------------                                                                                                                                                                                                                                                                 | ----------------------------------------                                                                                                                     | ----------------------------------------                                                 |
| 可否不在沙箱內運行                       | ✅  可以                                                                                                                                                                                                                                                                                                 | ✅  可以 (若snap構建時使用且允許使用'classic'限制模式) [[1](https://docs.snapcraft.io/reference/confinement)] [[2](https://youtu.be/WQ9LSZ7_6QM?t=1h10m52s)] | :x: 不可以                                                                               |
| 可以使用其他沙箱                         | ✅  可以 (如[Firejail](https://github.com/netblue30/firejail) [[1](https://firejail.wordpress.com/documentation-2/appimage-support/)], AppArmor [[2](https://github.com/netblue30/firejail/commit/1738bbf7181d6c3b6d9f82bfa5b3f6d21ad503c3)], [Bubblewrap](https://github.com/projectatomic/bubblewrap)) | :x: 不可以 (與AppArmor緊密耦合/集成)                                                                                                                  | :x: 不可以 (與[Bubblewrap](https://github.com/projectatomic/bubblewrap)緊密耦合/集成) |

安裝與運行
-----------------

| **特性**                                               | **AppImage**                                    | **Snap**                                                             | **Flatpak**                                                                                                      |
| ----------------------------------------               | ----------------------------------------        | ----------------------------------------                             | ----------------------------------------                                                                         |
| 可否不安裝就執行                                       | ✅  可以 (設置「可執行」位即可)                 | :x: 不可以 (需要通過snapd安裝)                                       | :x: 不可以 (需要通過Flatpak的用戶側工具)                                                                         |
| 可否不要求root權限                                     | ✅  可以                                        | ⚠️ 僅在安裝後可以                                                     | ⚠️ 僅在安裝後可以                                                                                                 |
| 可否從壓縮的源（文件）執行且不需要解包                 | ✅  可以                                        | ✅  可以                                                             | :x: 不可以                                                                                                       |
| 作者可以直接放置離線安裝文件的下載                     | ✅  是 (.appimage - 包含離線執行程序的所有內容) | :x: 不可 (.snap - 須裝有snapd，且若需要額外安裝其他snap則系統須在線) | :x: No (.flatpakref文件需要聯網；.flatpak捆束需要安裝一個運行時)                                                 |
| 應用程序作者可否自行伺服程序且無功能損失               | ✅  可以                                        | :x: 不可以                                                           | ✅  可以 [[1](http://docs.flatpak.org/en/latest/hosting-a-repository.html)]                                      |
| 適用於/被優化適合air-gapped（離線）機器                | ✅  是                                          | :x: 並不                                                             | ✅  是 (P2P支持允許離線安裝和升級)                                                                               |
| 可否在非標準位置（如網絡共享、CD-ROM等）存儲和運行程序 | ✅  可以                                        | 尚未確定                                                             | ✅  可以 (需要配置) [[1](http://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-installation)] |


應用分發
------------

| **特性**                                 | **AppImage**                                    | **Snap**                                                                                               | **Flatpak**                                          |
| ---------------------------------------- | ----------------------------------------        | ----------------------------------------                                                               | ----------------------------------------             |
| 核心倉庫/目錄                            | [AppImageHub](https://appimage.github.io/apps/) | [Snap Store](https://uappexplorer.com/snaps)                                                           | [FlatHub](https://flathub.org/apps.html)             |
| 完全去中心化，無中心gatekeeper           | ✅  是                                          | :x: 不是 (唯一支配性軟件商城) [[1](https://medium.com/@uriadonayherrera/nitrux-appimage-52937e286edc)] | ✅  是                                               |
| 多版本共存 (及歷史版本)                  | ✅  是 (無任何限制)                             | ✅  是 (每channel一個)                                                                                 | ✅  是 (OSTree上的任意版本可共存)                    |
| 安裝後可分享給他處                       | ✅  是 (文件即應用，不存在「安裝」前後)         | ✅  是 (但仍需要複製其依賴)                                                                            | ✅  是 (可以使用`flatpak create-usb`複製到USB存儲器) |

更新
--------

| **特性**                                 | **AppImage**                                                                                                | **Snap**                                          | **Flatpak**                              |
| ---------------------------------------- | ----------------------------------------                                                                    | ----------------------------------------          | ---------------------------------------- |
| 更新機制                                 | [AppImageUpdate](https://github.com/AppImage/AppImageUpdate)                                                | 從倉庫                                            | 從倉庫                                   |
| 二進制差異更新                           | ✅  支持 (使用zsync故不需要預先生成差異)                                                                    | ✅  支持 (僅在使用私有服務端且服務要求生成差異時) | ✅  支持 (使用OStree來提供原子更新)      |
| 應用程序可以自行升級                     | ✅  支持 ([使用內嵌信息](https://github.com/AppImage/AppImageSpec/blob/master/draft.md#update-information)) | ⚠️ 若snapd已安裝                                   | ⚠️ 若FlatPak已安裝                        |

