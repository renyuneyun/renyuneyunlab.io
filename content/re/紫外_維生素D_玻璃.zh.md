Title: 玻璃窗嚴重影響維生素D合成
Date: 2020-07-06 11:23
Lang: zh
Category: re
Tags: 紫外, 維生素, 玻璃, 光


紫外線
-------

紫外線按波長可分爲UVA、UVB、UVC等。

* UVA：波長320-420nm
* UVB：波長275-320nm
* UVC：波長200-275nm

https://zhidao.baidu.com/question/1431964362674984579.html


維生素D生成
-------------

由維生素D原吸收270-300nm波長光子轉化產生。

https://baike.baidu.com/item/维生素D/781038?ivk_sa=1022817p#3

實際來自「杨春华. 维生素D简介[J]. 中国实用医药, 2009, 4(3):243-244.」（[在線閱讀](https://www.ixueshu.com/document/5a841da6842b8356318947a18e7f9386.html)）

玻璃對紫外透過性
-------------------

常規家用玻璃對300nm波長以下的光透射度很低，在約320nm時透射度迅速攀升。

![家用玻璃透射光譜](https://pic4.zhimg.com/de1df99a97a070b77487cb47513d5814_r.jpg)

https://www.zhihu.com/question/20720208/answer/16165129

