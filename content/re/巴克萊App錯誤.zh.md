Title: 巴克萊App的一些錯誤代碼
Date: 2022-11-30 17:03
Lang: zh
Category: re
Tags: root, Android, error code
Slug: error-code-in-barclay-app

前一段開了張巴克萊銀行的卡。但開卡流程需要通過App完成，不能走銀行櫃檯。在流程中，我遇到了一些錯誤。這裏轉述一下其中兩個的原因。

## 00006

檢測到了TWRP目錄的存在。

> I found error code 00006 problem, Barclays app detects TWRP folder in internal memory, just delete it and it works

[Can Barclays App be used on rooted devices with Magisk? | Page 4 | XDA Forums](https://forum.xda-developers.com/t/can-barclays-app-be-used-on-rooted-devices-with-magisk.3779919/page-4) https://forum.xda-developers.com/t/can-barclays-app-be-used-on-rooted-devices-with-magisk.3779919/page-4

## T0020

App在安全文件夾/工作資料/應用雙開模式下工作。

[Problem with Barclays app on Secure Folder ](https://eu.community.samsung.com/t5/galaxy-s20-series/problem-with-barclays-app-on-secure-folder/td-p/1604969/page/2) https://eu.community.samsung.com/t5/galaxy-s20-series/problem-with-barclays-app-on-secure-folder/td-p/1604969/page/2
