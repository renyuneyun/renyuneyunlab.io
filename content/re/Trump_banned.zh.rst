現任美國總統特朗普的賬號被封禁
#####################################

:date: 2021-01-10 23:38
:lang: zh
:category: re
:tags: Twitter, 媒體, 陰, 特朗普
:summary: Twitter封禁現任美國總統唐納德·特朗普的個人賬號，且未給出細節原因


.. figure:: {static}/images/re/trump-banned/Twitter.20210110.png
   :alt: 當前狀況
   :width: 600

   川普賬號的當前狀況

.. figure:: {static}/images/re/trump-banned/Twitter.20210110_before.jpg
   :alt: Archive中的之前信息
   :width: 600

   從archive中找到的川普賬號之前的發言。不知道Twitter會否刪除發言且不留痕跡

.. figure:: {static}/images/re/trump-banned/Twitter.20210110_date_proof.png
   :alt: 日期說明
   :width: 600

   解釋上圖中的「2天前」是什麼意思

