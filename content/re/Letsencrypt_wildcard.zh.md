Title: Let's Encrypt爲通配符域名獲取證書必須通過DNS查問
Date: 2022-05-30 22:23
Lang: zh
Category: re
Tags: Let's Encrypt, SSL, HTTPS, 通配符


Let's Encrypt爲通配符域名獲取證書必須通過DNS查問的途徑。可以手動，也可以通過DNS提供商相關插件。

## 手動方法

```
certbot certonly --manual -d *.YOUR.DOMAIN.NAME
```

在回答常規問題後，會進行驗證。驗證需要修改你的DNS記錄，增加它所提示的TXT記錄，然後進行驗證。

https://www.geeksforgeeks.org/using-certbot-manually-for-ssl-certificates/

如果同時申請`*.YOUR.DOMAIN.NAME`和`YOUR.DOMAIN.NAME`的證書，你需要增加兩份TXT記錄。增加即可，它們可以共存。

https://community.letsencrypt.org/t/wildcard-dns-challenge-fails-due-to-duplicate-txt-record/157654

## 使用插件

從[Let's Encrypt的插件列表](https://certbot.eff.org/docs/using.html#dns-plugins)或[第三方插件列表](https://eff-certbot.readthedocs.io/en/stable/using.html#third-party-plugins)中找到你的DNS提供商對應插件，安裝。然後配置並獲取證書。

https://www.digitalocean.com/community/tutorials/how-to-create-let-s-encrypt-wildcard-certificates-with-certbot

