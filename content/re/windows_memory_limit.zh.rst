Windows內存上限限制
########################

:date: 2020-06-06 19:17
:lang: zh
:category: re
:tags: windows, windows 7, RAM, limit
:slut: windows-memory-upper-bound

Windows諸非服務器版本對內存上限的設定，其中多爲額外限制。將導致物理內存總大小正常識別，但「可用內存」爲限制的大小。

+------------------------------+-------+
| 版本                         | 上限  |
+==============================+=======+
| Win 任意 32 bit              | <=4GB |
| （不支持\ PAE_\ 的必然結果） |       |
+------------------------------+-------+
| Win 7 64 bit Home Basic      | 8GB   |
+------------------------------+-------+
| Win 7 64 bit Home Premium    | 16GB  |
+------------------------------+-------+
| Win 7 64 bit Ultimate        | 192GB |
+------------------------------+-------+
| Win 8 64 bit                 | 128GB |
+------------------------------+-------+
| Win 10 64 bit Home           | 128GB |
+------------------------------+-------+

.. _PAE: https://docs.microsoft.com/en-us/windows/win32/memory/physical-address-extension

見載於：

* https://docs.microsoft.com/en-us/windows/win32/memory/memory-limits-for-windows-releases （官方）
* https://www.compuram.de/blog/en/how-much-ram-can-be-addressed-under-the-current-32-bit-and-64-bit-operating-systems/
* https://support.microsoft.com/en-us/help/978610/the-usable-memory-may-be-less-than-the-installed-memory-on-windows-7-b （僅Win 7）
* https://www.zdnet.com/article/max-memory-limits-for-64-bit-windows-7/ （有錯誤：Win 7 Starter應當不存在64位版本）


