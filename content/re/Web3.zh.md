Title: 那些稱爲Web 3.0的東西
Date: 2022-05-31 22:23
Lang: zh
Category: re
Tags: Web, Internet, Semantic Web, Linked Data, Blockchain, Decentralization
Slug: those-so-called-web3

Web 3或者Web 3.0並沒有統一的標準，而且有不同的定義/詮釋，差別很大。

當下流行的是以區塊鏈爲核心的說辭，也即下面的第二種。

## W3C（World Wide Web Consortium）版

強調數據的互聯互通，以語義網（Semantic Web）/互聯數據（Linked Data）提供的互操作性和相關技術的推理能力爲核心。

![Semantic Web Architecture](https://www.w3.org/2006/Talks/1023-sb-W3CTechSemWeb/SemWebStack-tbl-2006a.png)

https://www.w3.org/2007/Talks/0123-sb-W3CEmergingTech/

https://en.wikipedia.org/wiki/Semantic_Web

## Web 3基金會（自稱）版

強調去中心化，用戶自治。
後文突然默認必須以區塊鏈及去中心化應用（dApp）爲核心來實現。

https://web3.foundation/about/

## 相關其他信息

### 碰瓷W3C

Twitter上有一個自稱爲Web 3 Collective的賬號，自行縮寫爲W3C，並[稱W3C說「區塊鏈和dApp是Web 3」](https://twitter.com/Web3_Collective/status/1474095199076814867)。

該賬號目前有1009關注者。

https://twitter.com/Web3_Collective

### 「Web 3好極了」

一個博客/時間軸，整理了區塊鏈流行所衍生的「萬物區塊鏈」下的各種問題，如各種刪號跑路和各種漏洞。

這裏的Web 3即目前流行理念，以區塊鏈爲核心的那種。

https://web3isgoinggreat.com/