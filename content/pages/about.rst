自述
#####

:slug: about

.. role:: raw_html(raw)
   :format: html

關於我
=======

一個Arch Linux用戶，傾向FLOSS。現下在牛津大學做博士後，相關信息見\ `這裏 <https://me.ryey.icu>`__ 。

常用網名便是「人云E云」（及其英文形態，如renyuneyun、ryey），可以在多種站點上找到我的存在。

  當年取「任云己之意欲云」之義，同時提醒自己不要「人云亦云」，以及要線上線下一致。

  到現在（2023年）爲止踐行得不錯。

其他地方的一些個人檔案：

* `WebID <https://renyuneyun.solidcommunity.net/profile/card#me>`_ (同時也用作測試)
* `項目與技術檔案 <https://me.ryey.icu/>`__
* `OpenHub檔案 <https://www.openhub.net/accounts/renyuneyun>`__

我的主要在線社交平臺：

* :raw_html:`<i class="fa fa-comments aria-hidden="true"></i>` `Matrix <https://matrix.to/#/@renyuneyun:matrix.org>`_

* 久未維護的舊賬號/平臺（因爲更多時間用在SoLiD上；另見我的\ `WebID`_\ ）

  * :raw_html:`<i class="fa fa-mastodon aria-hidden="true"></i>` :raw_html:`<a rel="me" href="https://pod.ryey.icu/@renyuneyun">Mastodon</a>`

    * :raw_html:`<i class="fa fa-globe aria-hidden="true"></i>` `HubZilla <https://hub.zilla.tech/channel/renyuneyun>`_ （支持ActivityPub協議，可和Mastodon互通）
    * Hubzilla屬於曾用系統，該實例已掛，且Hubzilla開發似乎進度不良，故回mastodon
  
  * `Twitter <https://twitter.com/renyuneyun>`_

關於本博客
===========

如君所見，本站並非一個純粹的技術博客，但其中相當多內容的確是關於技術無疑。
目前本博客的\ `分類 </categories>`__\ 主要是三個，分別爲：

- 信息技術：計算機等相關技術內容，是本博客的主要內容
- 生身世事：個人生活、社會話題或跨領域知識
- 如是我聞：遇到或蒐集到的特定信息，以呈現信息爲主，個人評註爲輔

出於\ `這些理由 <http://renyuneyun.is-programmer.com/2014/3/6/why_i_use_traditional_chinese.43312.html>`__\ ,新近（指自開始寫博客起，對於本博客新增內容來說則是全部）文章均由傳統漢字寫就。

`舊博客 <http://renyuneyun.is-programmer.com>`_\ 基本廢棄，但其中文章暫時還沒有遷移過來，評論亦同。有打算日後遷移過來，但一直比較懶。

本博客通過Matomo（貌似以前叫Piwik）統計訪問情況，尊重\ :abbr:`DNT (Do Not Track)`\ 設置。您的IP（v4）地址將\ :abbr:`僅保留前三段 (與255.255.255.0進行「與」操作)`\ ，以便進行統計，但同時尊重個人隱私。

本站是由Pelican生成的靜態網站，故需使用第三方評論機制，如Disqus或Hypothes.is；主題模板是自己定製的\ `修改版Simplify <https://github.com/renyuneyun/simplify-theme>`__\ ，增加了一些額外的插件支持以及進行了一些自定義。
