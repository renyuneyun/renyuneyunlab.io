項目展館
#########

:slug: projects

|easer-logo| Easer_
===================

事件驅動的Android自動化程序。可設置事件、狀況等爲觸發器，或建立邏輯連接，並在滿足條件時自動執行自定義的動作。

可傳遞事件內容至動作；支持擴展（通過獨立app）。

.. |easer-logo| image:: https://github.com/renyuneyun/Easer/raw/master/app/src/main/ic_launcher-web.png
   :height: 1.5em
   :alt: Easer logo

.. _Easer: https://renyuneyun.github.io/Easer/

ProjFS_
========

「映射」文件系統：將指定目錄的內容映射到目標掛載點，其中映射所用程序由用戶自定義。

便於自動轉換文件格式且同時保留原文件之類的任務。

.. _ProjFS: https://github.com/renyuneyun/projfs

pacman-pstate_
===============

類似openSUSE的 :code:`zypper ps` 命令，列出所有正在使用被更新過軟件包的（舊）文件的程序（及其所屬包）。

由pacman-ps項目衍生而來（原項目似已停滯，且原作者本擬改到perl），使用python重寫並加入新功能。

.. _pacman-pstate: https://gitlab.com/renyuneyun/pacman-ps

|camcov-logo| CamCov_
=====================

假裝半透明的屏幕：將（所有程序的）屏幕背景內容改爲攝像頭前的景象。

.. |camcov-logo| image:: https://github.com/renyuneyun/CamCov/raw/master/app/src/main/ic_launcher-web.png
   :height: 1.5em
   :alt: CamCov logo

.. _CamCov: https://github.com/renyuneyun/CamCov

|flock-logo| FLock_
===================

懸浮的鎖屏按鈕；同時可以長按發出某個特定的Broadcast。

.. |flock-logo| image:: https://github.com/renyuneyun/FLock/raw/master/app/src/main/ic_launcher-web.png
   :height: 1.5em
   :alt: CamCov logo

.. _Flock: https://github.com/renyuneyun/FLock

`Free Universe`_
=================

一個（分佈式）星系/天體運轉模擬及觀察/探索器。

.. _Free Universe: https://gitlab.com/FreeUniverse

