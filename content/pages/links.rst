友&鏈
######

:slug: links
:author_card: False


嘛……這個頁面本來應該是各種友人的。然而其中很少有我（前）身邊的朋友同學們，因爲他們往往沒有 :abbr:`自己的分享平臺 (主要就是博客啦)` 。

同學
=====

`jxlxy1995's blog <https://jxlxy1995.com/>`_

ArchlinuxCN社區友人
===================

`依云's Blog <https://blog.lilydjwg.me/>`_

`Phoenix's island <https://blog.phoenixlzx.com/>`_

`Farseerfc 的小窩 <https://farseerfc.me/>`_
