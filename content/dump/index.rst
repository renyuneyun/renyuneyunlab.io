順理成章
##############

:date: 2024-01-01 01:00

一些理念和思路的整理轉儲。掌握之時覺得如此自然，並不會特別意識到，但和人交流/爭執時纔發現並非如此。故記錄下來，方便有興趣者，也方便自己日後回顧審視。

