Title: 於arXiv上傳非支持LaTeX文檔，及雜
Date: 2021-10-03 19:59
Lang: zh
Slug: upload-non-supported-tex-doc-to-arxiv
Category: life
Tags: arXiv, LaTeX, XeLaTeX, PDF, 元數據, 學術

幾乎所有搞過（計算機）科研或考慮過論文和版權之類事項的人都知道，[arXiv](http://arxiv.org/)是一個用來分享論文預印本或其他學術類資料的非商業性網站。理所當然地，上傳到arXiv上的文檔也一般爲PDF格式的文件。

## arXiv上傳

事實上arXiv支持直接通過TeX源代碼編譯文檔，也支持提交PDF，但提交TeX源代碼是優先的。

![arXiv支持的文件類型]({static}/images/arXiv-file-types.png)

這麼做大概主要是爲了在TeX中自動插入一些指令，使得生成的PDF帶上arXiv的提交信息。
然而直接上傳PDF會使得這麼不好做，於是arXiv做了一個「巧妙」的設計：它會去嘗試檢測上傳的PDF文件，看它是不是由TeX生成的，如果是則提示你提交源代碼且拒絕你上傳。

一般而言，對於多數情況，如果你的文檔使用LaTeX或者pdfLaTeX編譯，那麼在arXiv上傳源代碼也挺好，沒什麼別的問題。

## 非支持LaTeX文檔

然而問題就在於二般情況：如果我的源文件不能用LaTeX或pdfLaTeX編譯，而需要用比如XeLaTeX或LuaLaTeX編譯呢？

這時候你會陷入一個兩難的境地：arXiv不支持這些編譯工具鏈，所以你上傳的源代碼沒法編譯通過，於是無法進行下一步；但假如你上傳自己編譯生成的PDF，arXiv又會檢測到你的文件是TeX生成的，要求你上傳源代碼！

我就遇上了這個情況。我需要使用XeLaTeX或LuaLaTeX的UTF-8支持，因爲我的文檔中包含非ASCII字符——我甚至都沒用到漢字，而只是帶重音符號的拉丁字母。我在文檔中直接包含了這些字符，因爲這些字符是用在諸如`lstlisting`的環境中，所以簡易的`\'{o}`方式不起作用。然而在使用pdfLaTeX編譯的時候，遇到它們會報錯。

於是結果就如上所述了。我知道一種解法是使用特殊的指令手動修正每一個字符，然而這會花費很多時間。於是我只得去搜尋解決方案。很顯然，我搜到了arXiv關於所支持工具鏈的說明，知道他們不支持且沒有仔細考慮去支持XeLaTeX或LuaLaTeX。

然後我找到了[這篇文章](https://www.monperrus.net/martin/how-to-use-lualatex-arxiv)，向LuaTeX用戶給出了簡明的指導，告訴讀者如何去除生成的PDF文檔內那些特徵信息。然而，我照着它說的做了，發現並沒有什麼作用。我也打開了它所[提及的另一個指導](https://tex.stackexchange.com/questions/95080/making-an-anonymous-pdf-file-using-pdflatex)，通讀了一遍，調整了自己的指令，仍然沒有效果。此外我還讀了幾個其他的問題，發現要麼方法類似但沒有作用，要麼用別的手段繞過問題。

後來我改變了思路，決定在源文件中進行調整之後，再手動清理一下PDF文件的元信息，看是否有效。支持我作出這個嘗試的理由是我查看PDF元信息的時候，發現它包含一段「生成者」字段，內容包含各種TeX信息。這個字段應該就是前面諸多指導試圖移除的內容，但在我這裏不知道爲什麼沒能刪除。（一個猜測是因爲我用的`acmart`樣式。）

於是我嘗試了幾個工具，一個是[mat2](https://0xacab.org/jvoisin/mat2)（在Archlinux的community倉庫中有），一個是[pdftag](https://github.com/arrufat/pdftag)（在AUR有）。首先用了mat2，因爲它有KDE Service Menu集成（即dolphin選中文件後上下文菜單/右鍵菜單中的動作），且號稱支持許多文件類型，大概比較好用。然而mat2只能一次性移除所有元信息，且不知是什麼原因，在我這裏生成的PDF文件打開後會被okular報錯（但還是能打開）。於是後來又轉向了pdftag，發現也是有圖形界面的，且可以自行決定調整哪些元信息。

然後我就將移除了TeX相關元信息的PDF上傳給了arXiv，檢查順利通過。而且進一步嘗試發現，我原本的PDF在移除了這些元信息後，直接就能順利通過arXiv的檢查……

## 雜

文章的最後，說一個題外話。我在搜索相關話題的時候，發現了[這個問題和回答](https://tex.stackexchange.com/questions/542224/why-is-there-no-xelatex-support-for-arxiv)。問問題的人確實很不客氣，但回答的人也很傲慢。摘錄一段最典型的部分：

	Nobody cares about your lovely ligatures, special symbols, fancy fonts, or magical markup. And while it's true that arXiv accepts submissions in languages other than English, nobody realistically does that.

在這個回答中，一股濃濃的英語中心主義（不知道有沒有這個詞）氣息撲面而來。確實現在的科研主流語言是英語，但這也只是「現在」——歷史上，科研通用語換過幾次，英語只是最近的通用語。

我不知道如果時移事遷，科研通用語再次改變，到時候這個回答者會是何種感受？我也不知道這個回答者的專業是什麼，但有許多研究領域至少研究對象包含其他語言，需要在論文中包含它們對應內容，這個人對這些研究領域又是什麼態度？

也許回答者並沒有深入思考，但無心之言反而最能體現一個人內心深處的想法，甚至可以反映一個社會或羣體的想法。
