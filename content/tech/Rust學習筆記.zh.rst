Rust學習筆記
#############

:date: 2018-6-9 20:50
:modified: 2018-8-7 00:23
:lang: zh
:category: 信息技術
:tags: 筆記, 編程語言, 教程, rust
:series: Rust學習筆記
:slug: rust-note
:series_index: 01


------

    **注意** ： 部分章節尚未完工

在糾結了兩三年後，今年我終於下定決心好好學一下Rust。巧的是我還看到了Increasing Rust's Reach 2018，報名然後入選了，正好以此作爲契機寫一篇不一樣的Rust教程。

    本文主要目的是描述我學習Rust時的一些（絕大多數）心得和體會。其動機之一是給自己留個易於檢查的筆記，之二是提供一個針對特定程序員的Rust教程。

    該筆記適用於我及和我有類似編程背景的人。對該系列而言，所謂的“類似編程背景”主要是指C和OOP，以C、C++、Java、Python爲最佳。 


    不同編程語言所融合的特性不同，因而造就了不同語言的不同特色，但同時也使得學習一門新語言時不僅要學習語法還要學習自己之前所不知道的那部分特性。作爲一門編程語言，Rust中的許多特性和其他許多語言很類似，或乾脆就是一樣。因而，下文在介紹Rust時，如果是所遇到特性是編程背景中已知的部分，將會直接指向該背景特性而不加過多解釋；遇到類似概念，會給出不同之處；遇到新概念則當然是較爲詳細解釋。 

我爲什麼學習Rust
================

同絕大多數語言一樣，Rust是一門命令式語言；同絕大多數“現代”語言一樣，Rust具有命名空間機制；由於目標是系統編程語言，Rust是一門編譯型語言（但較爲強調編譯速度）。

在Rust的所有特點中，我比較在意的幾個（核心）特點是：

* 編譯型
* 手動類型+自動類型推導
* 非手動內存管理
* 無空指針風險
* 多值返回
* 支持泛型
* 支持運算符重載
* 支持高階函數（及匿名函數）
* 支持結構化的宏（參考lisp系語言）

這些可以說是我選擇去學習Rust的核心動力——這些特性在我的編程背景中幾乎全部存在，但都是散落的，從來沒有集中出現在某一個語言中——而Rust將這些我最想要的功能集合在了一起。

章節
====

筆記系列基本以我認爲比較合理的順序進行排序，希望該順序也適合本文其他讀者。

* `基礎 <|filename|rust-note/basic.zh.rst>`_
* crate
* `數據類型 <|filename|rust-note/data-type.zh.rst>`_
* `基本控制流程 <|filename|rust-note/basic-control-flow-and-enum.zh.rst>`_
* `所有權+引用+借用 <|filename|rust-note/ownership_reference_borrow.zh.rst>`_
* `壽元 <|filename|rust-note/lifetime.zh.rst>`_
* `結構體和方法 <|filename|rust-note/struct-and-method.zh.rst>`_ (未完)
* trait
* 再敘enum和模式匹配
* 泛型
* `堆棧在Rust中的映像 <|filename|rust-note/stack-and-heap-in-rust.zh.rst>`_
* 高級

暫未歸類相關內容
================

* `背景/適合人羣 <|filename|rust-note/background.zh.rst>`_
* `複合數據類型 <|filename|rust-note/compound-types.zh.rst>`_
