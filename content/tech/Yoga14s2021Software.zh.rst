Yoga 14s 2021上Arch Linux安裝細節及軟件（下）
##############################################

:date: 2022-07-09 11:41
:slug: archlinux-and-software-on-yoga-14s-2021-part-2
:lang: zh
:category: 信息技術
:tags: laptop, linux, Plasma, KDE, Howdy, plymouth, multi touch, firewall, Anbox, 美化

.. include:: ./_common_include.rst
.. include:: ./_arch_include.rst

\ `上篇 <archlinux-and-software-on-yoga-14s-2021-part-1>`_\ 講了在當前這臺筆記本電腦（Yoga 14s 2021 ITL）上系統安裝相關的一些細節。本篇聊聊我所用的一些用戶側的軟件，其中有不少都是由於新硬件帶來的，還有一些則是美化。

	本文大部分作於7月9日。然而最後一部分修修改改，就到了19日……以後還是一點一點一個話題一個話題的寫，不寫這種長的了……

開關機動畫（plymouth）
======================

有過其他發行版經歷的人，應該都看到過發行版相應的開關機動畫。它們是通過一個叫做\ `Plymouth <https://www.freedesktop.org/wiki/Software/Plymouth/>`_\ 的軟件完成的（當然也需要對應的內核機制支持，但這個現在基本不用操心）。

.. note::

	內核部分，似乎主要是得支持\ :abbr:`KMS (Kernel Model Setting)`\ 。我以前用Acer 5745G的時候，由於有個Geforce GT 330M獨顯，啓動時似乎沒法啓用KMS，所以一直沒能成功啓用plymouth。

它的安裝和配置過程在對應\ `wiki頁面 <https://wiki.archlinux.org/title/Plymouth>`__\ 寫得很詳細。
安裝沒有什麼特別的，按照正常方式即可。在AUR有其對應的包，同時\ |archlinuxcn repo|\ 也有。而安裝後需要進行一些設置，不然沒法使用。

安裝完畢後，首先需要編輯\ :path:`/etc/mkinitcpio.conf`\ 來將\ :code:`plymouth`\ 加入\ :code:`HOOKS`\ （參見\ `wiki頁面這一節 <https://wiki.archlinux.org/title/Plymouth#The_plymouth_hook>`__\ ），然後重新製作initramfs：

.. code::

	# mkinitcpio -P

.. note::

	關於mkinitcpio，可以參考\ `這個wiki頁面 <https://wiki.archlinux.org/title/Mkinitcpio>`__\ 。

在此之外，還需要編輯內核參數，需要增加\ :code:`quiet splash`\ 這兩者（在上篇中提到過）。其wiki頁面還說需要增加\ :code:`vt.global_cursor_default=0`\ ，不過在我這似乎沒有區別。

這樣其實就已經啓用了。再然後就是一些細節的調整了，可以參照wiki找尋自己喜歡的。我主要調整了這些：

1. 主題選用arch-charge，會顯示一個逐漸填滿（在關機時則是變空）然後閃亮的Arch的logo。
2. 按wiki所說，啓用了sddm-plymouth這個systemd服務，以期望流暢轉換。然而這一步對我似乎沒有任何區別，也沒能找到別的方案。
3. 調整HiDPI縮放爲2倍。

類似Windows Hello的人臉認證（Howdy）
=======================================

這臺電腦有一個我以前電腦都沒有的功能（設備），就是紅外補光的攝像頭。它同時也是爲了支持Windows Hello，就是那個可以通過人臉認證來登錄、認證及解鎖的功能。
（不知道它是否需要及/或支持所謂的結構光？）

當然這個功能在Windows上存在了幾年了，但仍然並不屬於我之前的電腦所具有的功能，於是之前也就沒配置過。這回碰上了，自然要好好利用，在Linux上也支持之。

抱着這個目的，我找到了\ `Howdy <https://github.com/boltgolt/howdy>`_\ 這個軟件。它的說明非常直白：「Linux上的Windows Hello」。

內核顯示檢測到了兩個攝像頭，一個是彩色的（\ :path:`/dev/video0`\ ，名稱叫\ :code:`Integrated C`\ ，其中C應當是代表color）一個是紅外的（\ :path:`/dev/video2`\ ，名稱叫\ :code:`Integrated I`\ ，其中I應當是代表infrered）。所以我直接去裝了Howdy。

> 很有意思的是，我這裏還有video1和video3這兩個額外的設備。但不知道它們是做什麼的，也沒法讀出數據。

裝上Howdy並測試時纔發現，我的紅外補光燈在Linux下居然無法打開！於是紅外攝像頭的畫面幾乎無法分辨任何東西。雖然彩色攝像頭版的也可以用，但紅外有在夜間也能正常使用的優勢，所以還是希望可以用它。

然後反覆蒐找，終於找到了\ `linux-enable-ir-emitter <https://github.com/EmixamPP/linux-enable-ir-emitter>`_\ 這個腳本，可以開啓紅外補光燈。

需要注意的是，這個腳本會嘗試各種已知配置，來尋找當前電腦所支持的模式，而不是具有某種完全通用的機制來支持所有設備。很幸運，我的電腦在最初的版本就有支持的。

說一個題外話：我看到的時候正值該項目剛發佈，我也就給這個項目寫了PKGBUILD，然後在AUR創建並維護這個包（由於項目作者不熟悉Arch）。但後來項目作者始終希望在該腳本中處理一切，包括生成、安裝及管理其systemd服務，而似乎不太喜歡（或者不太理解）使用靜態的服務文件加上配置文件的模式。於是在3.2之後的版本中，該項目不再提供systemd服務文件。我也是在那前後不再爲主負責其AUR包，且我在自己系統中標記了不去升級更新版本。

配置好了紅外補光燈，就可以切換Howdy到對應的攝像頭了——紅外補光燈會自動亮起（閃爍）。然後重新訓練我的面部模型，這樣Howdy本體部分就配置好了。

但這還不夠，我還需要編輯PAM配置文件以允許在適當的時候通過Howdy驗證。整體而言，類似Windows Hello，在Linux上有這麼幾個地方比較有必要支持：

1. 登錄
2. 解鎖
3. sudo認證/鑑權時
4. polkit認證/鑑權時

例如，爲了在進行sudo時使用Howdy來認證，需要修改\ :path:`/etc/pam.d/sudo`\ ，在其中加上這麼一句：

.. code::

	auth      sufficient    pam_python.so /lib/security/howdy/pam.py

其他的部分使用Howdy也類似：

- 登錄認證需要修改對應的\ :abbr:`DM (Display Manager)`\ 的文件；
- 解鎖認證需要修改對應的桌面環境或鎖屏器的文件，如KDE Plasma就是修改\ :path:`/etc/pam.d/kde`\ ；
- polkit認證需要修改\ :path:`/etc/pam.d/polkit-1`\ 。

	由於我的HOME分區進行了加密，且配置了自動解密（需要密碼），於是登錄時我不能使用Howdy認證，也就沒修改SDDM的配置。另外，\ `這裏說 <https://gist.github.com/pastleo/76597c6ae8f95bb02982fea6df3a3ade>`__\ SDDM最好不要用Howdy。

注意這些文件中條目的順序會影響處理方式，因爲PAM是按從上到下的順序執行它們的。你可以在任意位置增加上述條目，但一個簡單的rule of thumb規則是：

- 如果希望一上來就用Howdy，那麼加到第一條；
- 如果希望先密碼驗證，如果失敗再用Howdy，那麼要保證上述Howdy條目前要有這麼幾行以首先嘗試密碼：

.. code::

	auth      sufficient    pam_unix.so   try_first_pass nullok

關於Howdy的配置資料目前有不少，但並不夠全面。我主要參考了其\ `wiki頁面這部分 <https://wiki.archlinux.org/title/Howdy#Setup_Howdy_to_start_when_needed>`__\ 以及\ `這個gist（KDE使用Howdy） <https://gist.github.com/pastleo/76597c6ae8f95bb02982fea6df3a3ade>`__\ 。

個人猜測，其資料缺乏的一個原因應當是PAM的資料缺乏。比如我最初就花費了不少時間去查PAM的資料，試圖弄明白其中加入的那些參數是什麼意思。後來最終弄明白，那些參數是傳給模塊的，要去查模塊的文檔而非PAM的文檔。比如\ :code:`try_first_pass nullok`\ 這兩個參數傳給了\ :code:`pam_unix.so`\ 這個模塊，那麼我就應該去看pam_unix的手冊，即\ :shell:`man pam_unix`\ 。

	從文檔看，這兩個參數對調整Howdy表現來說應該沒什麼意義。但我還是保留了，一是它們對我沒有害處，二是擔心萬一它們有什麼意義而只是我不知道。

自動亮度
==========

說到攝像頭，那麼自然也要說說光線傳感器以及自動調整屏幕亮度。

我之前在用XPS 13的時候，由於並不自帶光線傳感器但又想用自動亮度，於是使用\ |calise|_\ ——它通過分析抓取的照片來估計環境亮度，並設置屏幕亮度。由於是估計，時不時會出現錯誤。

到了Yoga 14s，有了內置的光線傳感器，可以擺脫估計錯誤的情況了。那麼就需要找相應工具來正確利用它。

	雖然不會有估計錯誤了，但仍然存在前後光線不一致的情況。不過這就不是在傳感器本身效果上面下工夫能解決的了。最近這些年有一些手機逐漸配備了前後雙光線傳感器，應當可以改善這個問題。

在找任何工具前，首先要確定自己的光線傳感器被正確識別且有讀數。這一步着實花費了我不少時間。最終發現，其信息在\ :path:`/sys/bus/iio/devices/iio:device0/`\ 之下。其中\ :path:`in_illuminance_raw`\ 即是我要的讀數。

然後就簡單多了：尋找工具以

1. 監測讀數並估測合適屏幕亮度；
2. 設置屏幕亮度。

我找到了\ `autolight <https://gitlab.com/craftyguy/autolight>`_\ 這個工具來監測傳感器讀數並估測合適亮度。但這個工具有一些bug（最要命的是在特定情況下無法更改屏幕亮度），且許多年沒更新。最初我手動修復了這些問題，且打算增加其他計算函數。但後來發現有\ `smartlight <https://github.com/ArchieMeng/smartlight>`_\ 這個活躍的fork（當然現在也有一年沒更新了），就切換到了它上面。

至於設置屏幕亮度，則是按照autolight/smartlight的說明，使用了\ `Light <https://github.com/haikarainen/light>`_\ 。當然，由於autolight的bug，我一開始手動測試了一段時間看light是不是有問題……

總而言之，UNIX的一切皆文件（Everything is file）原則的優勢體現得很明顯，不需要編程就可以檢查傳感器是否被識別，讀數是否合理，以及編寫工具去利用傳感器數據。但該小衆需求缺乏文檔使得該優勢並不能在一開始立即發揮出來。

觸摸板多點手勢（touchegg）
============================

之前在用XPS 13 （9343）時，就\ `已經折騰過觸摸板的多點手勢 <renyuneyun.is-programmer.com/posts/208385.html>`__\ 。當時的方法比較繁瑣，需要安裝並切換使用\ `xf86-input-mtrack <https://github.com/p2rkw/xf86-input-mtrack>`_\ 驅動，並進行系統全局設置。

這臺Yoga 14s的觸摸板也還算不錯，手感接近XPS 13的。同時它支持（至少）四指觸控，而我的XPS 13似乎只能支持到三指。有這樣的條件，怎能不配置一下多指手勢？

現在已經不需要專門安裝特別的驅動，而可以直接使用常規的libinput，而且也不需要對驅動進行額外設置。取而代之的是，需要使用一個用戶層級的事件監聽及響應程序。我使用的是\ `touchégg <https://github.com/JoseExposito/touchegg>`_\ ，並使用\ `touché <https://github.com/JoseExposito/touche>`_\ 來配置它。

	出於省事考慮，下文將會使用普通的字母e。

需要注意的是，touchegg需要首先作爲守護進程/服務運行在後臺，監聽觸摸並轉換爲多點手勢事件（對應命令是\ :cmd:`touchegg --daemon`\ ）。它需要相應權限來監聽觸摸板的輸入事件，所以需要啓動配套的系統級服務：

.. code::

	# systemctl enable --now touchegg.service

然後在用戶層面，需要以客戶端形式啓動touchegg，以和前面啓動的服務交互。由於我用的是KDE Plasma (5)，它在啓動時會自動開啓該客戶端服務，不需要我操心。但假如不想登出再登入，或所用的DE/WM不支持，那麼可以手動啓動：

.. code::

	$ touchegg

但這時候也只會進行默認的響應，我沒弄明白究竟是哪些手勢。我直接使用了touche進行配置——touche提供圖形界面來對手勢進行配置。

.. image:: {static}/images/touche.png
	:alt: Touche的界面
	:width: 80%

這裏的可調項有很多。我主要是根據個人喜好，儘量保證語義統一，設置了這些：

- 三指是窗口操作
	- 向左向右是後退前進
	- 向上向下是最大化/還原和最小化
- 四指是工作區操作
	- 向左向右是調整窗口平鋪位置
	- 向上向下是切換工作區（虛擬桌面）
	- 向外是顯示桌面

防火牆配置
=================

從接觸並簡單學習Linux上的防火牆之後（大約2012年？），我就一直在用\ `iptables <https://www.netfilter.org/projects/iptables/index.html>`_\ ——它的理念還是很好理解的，個人場景也比較容易理清需求。後來\ `nftables <https://wiki.nftables.org/wiki-nftables/index.php/Main_Page>`_\ 替換了iptables，我便將自己電腦上的遷移了過去，但服務器的仍然保留。

	iptables和nftables都是建於內核裏的防火牆，都是\ `netfilter <https://netfilter.org/>`_\ 項目（在不同階段）的產物。據我所知，nftables的內核部分比iptables更靈活，功能上更強大（以便同時支持{ip,ip6,arp,eb}tables），同時提供向iptables的兼容層。表現上看，它們的主要區別在於上層用戶空間的程序（即iptables和nft命令）及語法（還有語義）的差別。

在此期間，我發現Ubuntu的服務器使用\ `ufw <https://help.ubuntu.com/community/UFW>`_\ 來作爲前端配置防火牆，而非是用iptables命令。它的背後仍然是內核的netfilter，但對於普通防火牆配置任務來說更好理解和使用一些。

在本回系統配置過程中，我專門搜了一下各種防火牆配置前端，尤其是具有圖形界面的那些。最後選用了\ `firewalld <https://firewalld.org/>`_\ 。

我的需求並不多（畢竟是個人用的），所以其實裸用nftables或iptables本身是足夠了的：

- 標準防火牆功能：按端口允許或封鎖傳入傳出連接，及允許相關連接
- 正常支持Docker、zerotier等會自己建立虛擬接口的軟件

在換用了firewalld之後，我額外獲得了這些用上的功能：

- 按網絡屬性（尤WiFi名稱）設置不同的防火牆規則（組）
- 設置不同的防火牆規則組
- 按照預置的服務/軟件來配置防火牆規則

當然，由於我還用了不少額外的軟件，預置的規則並不足夠，於是自己添加了幾個。不過在撰寫本文時發現，其中一些已經被加入官方預置服務規則中了。

Kodi的防火牆規則
-------------------

在給各個程序（服務）創建規則時，最麻煩的是給Kodi創建規則，因爲缺少文檔。

我的需求很明確：用\ `Kodi <https://kodi.tv/>`_\ 來接收Android的DLNA媒體投射（如各類視頻軟件的投屏）。

然而查了許久也沒有人指明其相關的端口是什麼或防火牆規則如何設置。
查到了一些相關討論，但沒有明確的結論。（參見我在arch wiki的\ `kodi的討論頁的留言 <https://wiki.archlinux.org/title/Talk:Kodi#Ports_for_UPnP%2FDLNA>`__\ 。）

最後綜合並嘗試了很久，發現需要開啓UDP 1900和TCP 1489端口。再追查下去，UDP 1900是UPnP的客戶端端口（很合理，因爲DLNA基於UPnP），但TCP 1489是什麼就沒弄明白了。

	我並不能完全保證這是正確的，因爲kodi的表現似乎並不一致。有可能我仍然漏了一些端口。而且其實DLNA、UPnP等協議的關係和細節我也沒有弄明白。

缺憾
------

按照上面設置，我的防火牆已經足夠日常使用了。但仍然存在一些缺憾。

其中最主要的，就是我希望可以按程序（或進程）來創建規則。然而這似乎並不被iptables或nftables等支持，於是也沒有希望被其配置前端支持。

確實，對於服務器來說，這個功能沒什麼意義。對於普通用戶來說，其或許根本沒有開啓防火牆的意識。但對於我這種日常+半專業使用人士來說，這就是一個很令人難受的問題——典型服務軟件的端口和使用一般都比較清晰，但一些日常軟件所需要開的端口並沒有足夠的文檔或討論來解釋。如果可以按程序來，那麼應當會省掉很多事情。

希望哪天有哪個框架可以補上這個缺憾。

Plasma配置
=============

由於有了更大的內存（16G），我便換回了KDE Plasma (5)，並嘗試了一些美化。

	除了美觀外，換到Plasma的部分因素是我的AwesomeWM配置無法簡單重用於新的電腦上（尤其是HiDPI的細節），另一部分因素是awesome不支持wayland而我在考慮什麼時候全盤換到wayland上。

	然而由於各種問題（主要是一些軟件的崩潰，以及桌面環境時不時出一些小毛病），直到現在我也沒換到wayland，而只是時不時去試試（然後發現還有問題）。

談到美化，主要就是調整主題，以及使用latte dock取代Plasma自己的面板。另外，我還採用了動態桌布。

初版風格
--------

.. image:: {static}/images/Plasma5+Orchis-dark+BlurGlassy+Layan.jpg
	:width: 90%
	:alt: Plasma5+Orchis-dark+BlurGlassy+Layan風格下的Plasma

我最初使用的是\ `Orchis <https://www.pling.com/p/1458915/>`_ (Dark)全局主題（不要忘記單獨設置Kvantum部分）。其主要好處是暗色調，似乎有助於眼睛。而且我也比較喜歡半透明效果，它也有提供。
鼠標指針使用的是\ `Layan指針 <https://www.pling.com/p/1365214/>`_\ 。

最初Orchis似乎並不能完整半透明，所以我搭了個\ `Glassy <https://www.pling.com/p/1355997/>`_\ 應用程序風格，使得如Dolphin等有毛玻璃特效。一開始看着還行，截圖也是這個時期進行的。但後來用用覺得不好看，換回了Orchis自己，發現半透明有了，只不過alpha值（不透明度）非常高。

然後面板用的是Plasma原生的面板，只不過我給拆成了三部分：頂部居中以作爲AppMenu，左側下部作爲任務調度區，底部居右顯示系統及全局信息。當然，它們都設置了自動隱藏或窗口可覆蓋以免擋住窗口。

這樣的好處是可以最大化利用屏幕空間。AppMenu使得菜單移至窗口之外，使得窗口可以顯示更多內容。

原生面板的自定義功能還是有限，而且右下角的系統托盤需要時不時將指針移動過去纔能看到，偶爾覺得不太方便。後來隨着再次主題及風格調整，我對面板也進行了大規模改變。

後期風格
----------

看久了暗色，就想換回亮色。後來發現了\ `Nova <https://www.pling.com/p/1659120/>`_\ 這一系列主題，終於決定還是將所有東西都改改。\ `整個系列 <https://www.pling.com/find/?search=nova&page=1&pci=121&t=nova>`__\ 有多個顏色可選，我選了\ `綠色款 <https://www.pling.com/p/1659904/>`__\ 。
除了配色和顯示風格的不同外，我還換成了Latte Dock及其面板。

.. image:: {static}/images/Plasma5+NovaGreen+Latte+Layan.png
	:width: 90%
	:alt: Plasma5+NovaGreen+Latte+Layan風格下的Plasma

我之前一直儘量避免Mac OS X風格的頂部全局面板，其中最初是純粹感情因素（不喜歡蘋果因而不喜歡向它靠的行爲），但後來也有了更多實際因素的考量：浪費寶貴的豎向空間。

.. note::
	這一設計在更早期屏幕是4:3的時代或許問題不大，但隨着2006年前後個人電腦全部轉向16:10或16:9的屏幕，這一設計佔用空間的問題就體現出來了。當然了，比起微軟那邊從Windows 7起更加巨大的任務欄來說，Mac OS X的頂部面板倒是更省空間一點。

Ubuntu的Unity桌面環境在這裏處理得就比較好。它很早（印象中是從Ubuntu 10.10上網本版本開始加入Unity那時候就是）就將最大化窗口的標題欄合併入頂部面板，而在後來也加入了AppMenu（全局菜單）的支持。這樣，顯示空間不會因爲有了頂部面板而被浪費，反而會有額外的全局信息欄。當然，Unity上也有一點\ :hidden:`缺陷 (C社：這是個feature！)`\ ：沒辦法將窗口標題欄放在頂部面板後邊，導致多窗口平鋪時必須浪費空間。（雖然我也沒設置好自動平鋪時不浪費，但至少可以手動來。）

而這回我之所以加入了頂部面板，則是由於之前發現了一些\ `窗口操作按鈕的plasmoid <https://www.pling.com/find/?search=window%20control&page=1&pci=105>`__\ （即Plasma小部件）。這回則是組合了Latte Dock作者所維護的幾個小部件，支持進行窗口操作，顯示應用程序名稱，以及顯示全局菜單。

.. note::

	它們還有一些有意思的組合用法，比如當光標進入和離開時顯示或不顯示標題和菜單。我更喜歡\ `Active Window Control <https://github.com/kotelnik/plasma-applet-active-window-control>`__\ （另一個類似部件）所支持的光標移入時顯示窗口操作，然而很可惜它對AppMenu的組合支持不夠好，會有空白。

頂部面板的其他部分大都很直觀：中間是日期時間；右側則依次是系統負載信息（CPU、內存、硬盤、網絡、溫度），托盤。而在托盤右邊則是\ `切換邊欄 <https://www.pling.com/p/1365044/>`__\ 的按鈕，以及圖中看不到但存在的\ `（Win7風）顯示桌面 <https://www.pling.com/p/1100895/>`__\ 。邊欄是Latte的一種特殊面板——平時隱藏但僅在被激活時顯示。邊欄按鈕就是爲了配合這個機制而存在的，可以用來激活（和隱藏）邊欄。更多細節請參考其pling頁面的視頻。

左側面板仍然和以前一樣，是任務調度用的。最主要的區別就是移動到了中間，並增加了一些常駐的按鈕。之前和底部對齊是因爲要放置「顯示桌面」按鈕，而它現在移到了頂欄上，那麼在這裏就不再需要了。

動態壁紙
----------

除了上述主題相關美化以外，我還使用了動態壁紙。這裏的動態並非指幻燈片式的切換，而是會根據時間變化。

我其實找到了兩個項目來實現這一功能：

1. `plasma5-wallpapers-dynamic <https://github.com/zzag/plasma5-wallpapers-dynamic>`__\ ，使用自己的基於動態圖片的格式（avif，之前爲heif/heic）。
2. `plasma-wallpapers-xml <https://github.com/easyteacher/plasma-wallpapers-xml>`_\ ，使用和Gnome動態壁紙相兼容的XML格式。

由於兩者所支持的格式不同，所以它們的壁紙庫也不同：

1. plasma5-wallpapers-dynamic的倉庫給出了一個\ `壁紙庫 <https://github.com/karmanyaahm/awesome-plasma5-dynamic-wallpapers>`__\ 。其中只有有限的幾個資源，且新舊格式互不兼容；但它們可以直接下載使用（因爲是單個文件）。
2. Gnome的XML格式壁紙則可以去看\ `這個壁紙庫 <https://github.com/saint-13/Linux_Dynamic_Wallpapers>`__\ 。其中包含許多壁紙；但需要按照結構安裝到本地纔能使用（我順便打了個\ `AUR包 <https://aur.archlinux.org/packages/linux-dynamic-wallpapers-git>`__\ ，但不知道上游不存在明確的license是否影響）。

在我看來，plasma5-wallpapers-dynamic切換格式並放棄支持舊格式一事比較迷，畢竟本來壁紙資源也有限。不過詢問後，作者說\ `精力有限所以不打算支持多格式 <https://github.com/zzag/plasma5-wallpapers-dynamic/issues/109>`__\ 。

另外，我偶然在KDE的倉庫發現了\ `這些提交 <https://invent.kde.org/plasma/plasma-workspace/-/commits/work/fuf/image/dynamic>`__\ ，似乎預示着KDE團隊想讓Gnome XML格式動態壁紙進入官方支持。

然而我簡單對比發現，avif格式的動態壁紙比heif格式的動態壁紙小一些，而二者都要比Gnome XML動態壁紙的小。這或許算是我對這些格式的最大怨念了……

運行Android程序（Anbox）
=========================

有點相關知識的人應當都知道，Android的底層就是Linux。猶記得當年有過Android算不算GNU/Linux或Android算不算Linux的討論，但基本上在一些代碼進入Linux內核後不再討論了——Android算Linux，但不算GNU/Linux。

既然算Linux，那麼按理說其他Linux也可以跑Android，只不過要裝備其非GNU的那套上層框架。這時候就可以請出\ `Anbox <https://anbox.io/>`_\ 了——它致力於直接在Linux上跑Android框架及應用程序，並將其限制在容器內以增強安全性。

不過要跑Android，就需要一些Android特有的內核特性。Anbox需要這兩個內核模塊：binder和ashmem。

注意這些內核模塊有時候沒法在一些（新）內核上編譯，因爲它們用了一些比較特別的調用，出於安全考慮會被禁用。如\ `上篇`_\ 所說，我用了linux-lily內核，所以不用再操心自行編譯的問題了。

當然了，要跑Android應用，仍然需要安裝對應的Android框架，在Anbox來說就是Android映像。基礎映像不帶任何軟件倉庫/商店，所以需要依賴adb安裝應用；當然也可以考慮使有帶Google服務的映像。

除此之外，Anbox就是正常使用，其實沒太多好說的。而且過了新鮮之後，我其實也不太經常用——國產沒Linux版的軟件（其Android版）一般也不支持x86，而外國沒Linux版的軟件大多有網頁版（或者我用不上）。多說一句：「支持x86」不是什麼需要他們特別處理的，因爲Android本身編程是支持任意平臺的（這也是他們一開始選Java的原因）；反倒是「不支持x86」是因爲他們做了什麼（一般應當是使用NDK，但只爲一小部分架構編譯）。

其他軟件及結語
==============

以上就是換到這臺電腦後對軟件和美化方面的一些個人折騰了。寫一寫以了自己對新近硬件上GNU/Linux使用體驗文章匱乏的感嘆。希望對讀者有用，吧……

折騰的時候發現了\ `Plasma Customization Saver <https://www.pling.com/p/1298955/>`_\ 這個小部件。它可以幫助迅速保存並切換Plasma配置風格。對經常想換換Plasma風格的人極其利好。

除了它們之外，我其實還嘗試了一些其他軟件。有的在以前的文章中寫過，有的則不知道該如何寫，因爲就那麼用就行。
不過還是簡單寫個推薦吧，畢竟當年剛接觸Linux不久的我應當會很希望看點這類經驗的（另可參考\ `Arch wiki上的應用程序列表頁面 <https://wiki.archlinux.org/title/List_of_applications>`_\ ）。如果讀者有更好的推薦，也可以改進我的工具鏈。

- 桌面操作Android：\ `scrcpy <https://github.com/Genymobile/scrcpy>`_\ 或\ `QtScrcpy <https://github.com/barry-ran/QtScrcpy>`_
	- 如果不工作的話，記得編輯其配置文件以指向正確的adb
- 桌面和Android多機同步：\ `KDEConnect <https://kdeconnect.kde.org/>`_\ （這個我其實從它剛誕生不久就開始在用，到現在應該有六七年了）
	- 不只是一對一，而是可以多對多
	- 還有開放文件系統（遠程掛載）這個我很常用的功能
		- 但由於Android 11（？）的限制，\ :path:`/sdcard/Android/`\ 沒法顯示了
- 筆記軟件：\ `trilium <https://github.com/zadam/trilium>`_
	- 我是拿它來當個人wiki用的
	- 以前試過\ `zim <https://zim-wiki.org/>`_\ 、\ `TiddlyWiki <https://tiddlywiki.com/>`_
	- 最大缺憾是數據存在數據庫內而非純文件
- Markdown編輯器：\ `MarkText <https://github.com/marktext/marktext>`_\ 和\ `Zettlr <https://www.zettlr.com/>`_
	- 都是帶預覽的Markdown編輯器，其中MarkText是所見即所得的
	- MarkText會強制Markdown文件爲它的格式化風格，所以我有時候會用Zettlr
	- 對於reStructuredText，我還是在用\ `ReText <https://github.com/retext-project/retext>`_\ ，因爲似乎沒有好的（帶預覽的）編輯器
- 工作記錄：\ `superProductivity <https://super-productivity.com/>`_
	- 記錄工作時長；會自動檢測是否在使用電腦（目前僅限X11）
	- 有多種同步方式
	- 最大缺憾是記錄不帶時間而僅有時長
- 密碼管理：\ `Bitwarden <https://bitwarden.com/>`_\ 或\ `Pass <https://www.passwordstore.org/>`_
	- Pass算是極簡風格但又有擴展性的密碼管理器
		- 純文件存儲，允許自定義字段；自動通過git維護歷史；使用GPG加密；支持多設備多密鑰
		- 可通過插件支持TOTP（許多動態驗證碼都是它）
		- 有瀏覽器插件提供檢索密碼庫並填寫
	- Bitwarden是一個開源的圖形界面的密碼（及其他類似私密信息）管理器
		- 它有自己的同步服務器；服務端開源，可自建；也有一些兼容服務器
		- 支持TOTP，有一些高級功能（如添加附件），但需要付費（不過\ :abbr:`不算貴 (一年$10)`\ ）；我不清楚自建服務器是否也需要付費，估計不用
		- 也有一些兼容的命令行客戶端
		- 官方提供瀏覽器插件，但插件需要登錄並會自己存儲密碼庫，而不是和系統上的進行交互
	- 另外還有一些其他的管理器，尤其是\ `KeePass <https://keepass.info/>`_\ 系列（維護的最好的似乎是\ `KeePassXC <https://keepassxc.org/>`_\ ）
		- 但它們的同步功能很有限，所以不在我的考慮之內
		- 而且對TOTP的支持不好（或者說沒有）
- 虛擬局域網：\ `Zerotier <https://www.zerotier.com/>`_
	- Zerotier可以讓網絡上兩個或多個設備處於同一個虛擬的局域網之中，擁有穩定的局域網IP地址
	- 和交換機上的VLAN並不同，而更接近programmable network
	- 對我來說，其主要用途是在IPv6並不普及的狀況下，允許我的兩臺（在不同網關後的）設備之間建立連接（或者說內網穿透）

好了，以上就是最近新用的以及以前沒找到合適地方寫的一些軟件了。其中許多都有替代品可用，我只是出於我的需求和喜好選了其中的一個，也許你們更喜歡別的也說不定呢？


.. |calise| replace:: :abbr:`calise (camera light sensor)`
.. _calise: http://calise.sourceforge.net/wordpress/