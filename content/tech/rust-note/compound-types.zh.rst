Rust學習筆記/複合數據類型
#########################

:date: 2018-7-15 22:34
:series: Rust學習筆記
:lang: zh
:category: 信息技術
:slug: rust-note_compound-types
:series_index: 05

.. include:: ../_common_include.rst

Rust提供元組和數組兩種複合數據類型。

元組
====

元組可將一系列數據（不需要是相同類型）“綁”在一起，以方便後續處理。

.. code:: rust

   fn main() {
       let tup: (i32, f64, u8) = (500, 6.4, 1);
       
       let (x, y, z) = tup;
   
       println!("The value of y is: {}", y);
       
       let five_hundred = tup.0;
   
       let six_point_four = tup.1;
   
       let one = tup.2;
   }

（抄自 `官方教程元組部分 <https://doc.rust-lang.org/book/second-edition/ch03-02-data-types.html#the-tuple-type>`_ ）

* 使用 :rust:`()` 將多個數據合在一起組成元組
* 元組的類型依賴於其各組成元素的數據類型
* 解包操作可以一次性將元組中所有元素賦予某個值
* 使用 :rust:`.N` 訪問元組第 :rust:`N+1` 位的元素

數組
====

數組的概念和其他語言一樣，但其賦值語法更像Python的列表：

.. code:: rust

   fn main() {
       let a = [1, 2, 3, 4, 5];
   
       let first = a[0];
       let second = a[1];
       
       let index = 10;
   
       let element = a[index];
   
       println!("The value of element is: {}", element);
   }

當嘗試訪問不存在的元素時，編譯器不會報錯，但運行時會報錯。 
