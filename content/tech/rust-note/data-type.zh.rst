Rust學習筆記/數據類型
######################

:date: 2018-7-8 00:43
:modified: 2018-7-15 22:44
:series: Rust學習筆記
:lang: zh
:category: 信息技術
:slug: rust-note_data-type
:series_index: 04

.. include:: ../_common_include.rst

類似於絕大多數語言，Rust核心部分提供的數據類型包含整型、實型（浮點數）等常規類型，同時也提供字符串類型。

標量/單量類型
=============

整數與浮點數
------------

:rust:`i8` 是8位（有符號）整型， :rust:`i32` 是32位整型； :rust:`u32` 是32位無符號整型； :rust:`f32` 是32位實型（浮點數）。 其具體大小均包括8、16、32、64這四種。 額外地，對於整數，另有 :rust:`isize` 和 :rust:`usize` 兩種，代表“取決於機器的大小”。

對於整數，Rust也支持許多其他“單位”的轉換：二進制、八進制、十六進制、以_分段的十進制。 額外地，對於 :rust:`u8` ，Rust支持直接賦予ASCII對應值（如 :rust:`b'A'` ）。例子及更多解釋見 `官方教程整數類型部分 <https://doc.rust-lang.org/book/second-edition/ch03-02-data-types.html#integer-types>`_ 。

算數運算符和其他C家族的一致，不再贅述。

布爾類型
--------

布爾是單獨的類型（ :rust:`bool` ），其值爲 :rust:`true` 或 :rust:`false`\ 。

不同於C等語言，Rust不認爲 :rust:`0` 是 :rust:`false` 、 :rust:`1` 是 :rust:`true` 。

字符類型
--------

一個字符類型（ :rust:`char` ）的變量表示一個Unicode Scalar Value（基本可以理解爲一個Unicode字符）。

Rust區分字符和字符串，（同多數語言一樣）其字面量分別使用單引號和雙引號。

字符串
======

Rust中的字符串分爲 |str| 和 |String| 兩種。其區別是由於Rust目的（作爲系統編程語言，取代C）： |str| 在 :abbr:`棧 (stack)` 上分配，而 |String| 在 :abbr:`堆 (heap)` 上分配（該類型更像是指針/引用）。

.. |str| replace:: :rust:`str` 
.. |String| replace:: :rust:`String` 
.. |&str| replace:: :rust:`&str` 

另外， |str| 不可變，而 |String| （在其 :rust:`mut` 時）可變。

使用 :rust:`"abcd"` 創建的是 |str| ，但兩者之間可以轉換： :rust:`str.to_string()` 和 :rust:`String.as_str()` 。後文會更詳細介紹。

複合數據類型
============

Rust提供元組和數組兩種複合數據類型。

這兩者對快速入門幫助不大，但有興趣者可以閱讀 `Rust學習筆記/複合數據類型 <|filename|compound-types.zh.rst>`_ 。 
