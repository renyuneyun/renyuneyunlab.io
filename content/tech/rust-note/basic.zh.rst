Rust學習筆記/基礎
#################

:date: 2018-6-14 14:32
:modified: 2018-7-8 01:01
:series: Rust學習筆記
:lang: zh
:category: 信息技術
:slug: rust-note_basic
:series_index: 03

.. include:: ../_common_include.rst


包管理器Cargo的基本使用
=======================

Rust官方提供了包管理器Cargo，且Cargo同時負責項目的初始化、構建等功能。建議的做法是使用Cargo初始化項目，然後進行編寫，之後使用Cargo進行構建。故而，這裏簡單介紹如何用Cargo完成這兩項功能。

創建項目
--------

.. code:: shell

   $ cargo new YOUR_DIR --bin

該命令會在當前目錄下生成 :cmd:`YOUR_DIR` 這個新目錄，其中包含新項目的“模板”文件（即示例）。之後的命令基本均在該目錄下進行。

注意這裏的 :cmd:`--bin` 要求Cargo創建一個二進制/可執行項目； :cmd:`--lib` 或留空均會創建庫項目。

在 :cmd:`YOUR_DIR` 目錄下有一個名爲 :cmd:`Cargo.toml` 的文件。該文件是項目的（Cargo）配置文件，用來描述項目的依賴關係、控制編譯/構建過程等。

編譯並執行項目
--------------

.. code:: shell

   $ cargo run

Rust基本語法
============

Rust是一門強類型、手動類型的語言，這意味着函數參數需要顯式地聲明參數類型（以及返回值類型）。但Rust支持類型自動推導，在聲明並初始化變量時可以通過類型自動推導而省略變量類型。整體而言，Rust的語法承襲自C家族。

Hello world
------------

類似於C，Rust程序開始於 :rust:`main` 函數：

.. code:: rust

   fn main() {
       println!("Hello, world!");
   }

在Rust中，:rust:`main` 函數是一個沒有參數沒有返回值的函數。可以看到：

* :rust:`fn` 聲明一個函數
* 大括號/花括號 :rust:`{}` 用來標記代碼塊
* 分號 :rust:`;` 作爲語句結束符

注意到這裏輸出所用的 :rust:`println!()` 是一個 |macro| ——其中的感歎號 :rust:`!` 正是宏的標識（定義宏時編譯器自動添加）。Rust的 |macro| 更貼近Lisp系的宏，不在本文中介紹。

.. |macro| replace:: :abbr:`宏 (macro)`

函數、參數及類型聲明
--------------------

.. code:: rust

   fn my_awesome_function(x: i32) -> i32 {
       x + 1
   }

* Rust的數據類型在變量名稱之後，使用冒號分隔
* 函數返回值使用 :rust:`->` 標記
* 函數最後一行 **表達式** 作爲函數的返回值
   * 但也可使用return，寫成 **語句**

注意 **表達式** 和 **語句** 的區別，語句不返回值。

變量聲明及代碼塊
----------------

（部分代碼段抄自 |TRPL|_ ）

.. |TRPL| replace:: The Rust Programming Language (Second Edition)
.. _TRPL: https://doc.rust-lang.org/book/second-edition/ch03-03-how-functions-work.html#function-bodies-contain-statements-and-expressions

.. code:: rust

   fn main() {
       let a: float32;
       a = 30.4;
   
       let x = 5;
   
       let y = {
           let x = 3;
           x + 1
       };
       
       println!("a={} x={} y={}", a, x, y);
   }


* 使用 :rust:`let` 進行變量的聲明
   * 如之前所說，變量的類型在變量名之後
   * 當編譯器可以進行自動推導時，變量的類型可以省略
* Rust使用 :rust:`{}` 劃分代碼塊，每個代碼塊劃分一個作用域
* 內部代碼塊/作用域可以通過再次聲明變量，以臨時覆蓋外部變量的值
* 每個代碼塊被視爲一個 **表達式** ，（同函數一樣）使用其內部的最後一個表達式作爲其值

可變性
------

.. code:: rust

   fn main() {
       let x = 5;
       x = 1; // 本行會報錯，刪除本行解決問題
   
       let mut y = 2;
       y = 8;
       
       println!("x={} y={}", x, y);
   }

* Rust的變量默認爲 **不可變** （類似於const/final），函數參數也是如此
* 如要使其可變，需要在其名稱前面加上 :rust:`mut`

雜項（可不看）
--------------

.. code:: rust

   fn multi() -> (i32, i32, f32) {
       let x: i32;
       let y = 3;
       let z;
       x = 20;
       z = 50.0;
       (x, y, z)
   }
   
   fn main() {
       let (a, b, c) = multi();
       println!("a={0} b={1} c={2}", a, b, c);
   }

* 函數的聲明/定義順序不重要
   * 並且Rust似乎並沒有專門的“聲明”語法
* 函數可以進行多值返回，多個返回值使用 :rust:`()` 括起來
* 可以使用“解包”操作將多個值直接賦值給相同數量的變量
   * 這裏實際上是返回了一個 :abbr:`元組 (tuple)` （見 *數據類型* 部分）
* 已聲明的變量可以被重新聲明，以覆蓋原有的變量（常用於轉換類型）
