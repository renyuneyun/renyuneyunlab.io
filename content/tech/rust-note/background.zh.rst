Rust學習筆記/背景
##################

:date: 2018-6-9 21:10
:series: Rust學習筆記
:lang: zh
:category: 信息技術
:slug: rust-note_background
:series_index: 02

.. include:: ../_common_include.rst

對該教程而言，所謂的“類似編程背景”是指：C、C++ 98、Java 6、Python。Rust的部分概念覆蓋這幾種語言中各自的一部分，大約主要是如下部分（除去“編程”這個概念）：

* C、C++、Java的手動類型
* C++、Python的自動類型推導
* C、C++的指針
* C++中指針和引用的區別
* C++的const函數（ :rust:`int A::fn() const {}` ）
* Java、Python的自動內存管理
* C++、Python的lambda表達式
* C、C++、Python的高階函數
* C++、Java、Python的命名空間/包/模塊
* 與類型/結構體綁定的函數（半“面向對象”）
