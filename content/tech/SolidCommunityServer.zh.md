Title: 搭建Solid Community Server
Date: 2022-03-13 17:26
Category: tech
Tags: Solid, Decentralization, Linked Data, Semantic Web
Slug: solid-community-server-setup
Lang: zh

在[上一篇博文（《Solid——簡介與體驗》）](solid-introduction)中，我簡單介紹了Solid的一些基礎概念以及體驗它所可能遇到的障礙。在其中，我們提到了自行搭建Solid服務器這個選項——出於各種考慮，我們也許會傾向於自行搭建服務器。這篇文章以[Solid Community Server](https://github.com/CommunitySolidServer/CommunitySolidServer)爲例介紹如何自行搭建Solid服務器。

Solid Community Server（簡稱爲SCS或CSS）是Solid社區最新近構建的Solid服務器軟件。其最主要的特點是更佳的模塊化，以及更容易定製。大約正是因爲這些原因，所以社區在嘗試將以前更廣泛使用的[Node Solid Server（NSS）](https://github.com/solid/node-solid-server)切換爲CSS。CSS的定製化所依靠的是[Components.js](https://componentsjs.readthedocs.io/en/latest/)這個庫，不過本文不需要涉及它的細節。

## 須知與結構

本文將介紹如何在一個Ubuntu 20.04服務器上搭建CSS，會涵蓋從頭開始到可以完整訪問服務器。之所以提到服務器的具體發行版以及版本號，是因爲兩點：
- CSS[依賴於12.7版本](https://github.com/solid/community-server#-running-the-server)以上的Node.js；而Ubuntu 20.04倉庫中的node.js版本較低，是10.19，所以需要額外處理。
- 不同發行版的防火牆配置工具有所差異。
但原則上，除了這些以外，本文內容適合任何發行版。

由於Solid需要完整的URL以及HTTPS來正常工作，所以本文假設你擁有一個域名（假設叫`DOMAIN.NAME`），且服務器可以被公網正常訪問。

本文主要內容（下一節）圍繞搭建CSS服務器的具體步驟展開，主要分爲四部分：

1. 構建及運行CSS服務器；
2. 設置Web服務器（Nginx）；
3. 聯接Web服務器和CSS服務器（使用反向代理）；
4. 試用CSS的不同配置/默認App。

本文會介紹以及解釋每一個步驟，以期不同環境配置的讀者都可以用得上。在一些不重要的地方（比如使用Let's Encrypt獲取證書過程中的具體回答），本文會介紹要點，但跳過細節。

## 搭建指導

### 搭建CSS本體

取決於你的傾向，你可以選擇從npm倉庫或從源代碼構建CSS（參見[官方文檔](https://github.com/solid/community-server#-running-the-server)）。本文選擇從npm倉庫安裝CSS。

#### 更新node.js版本

如前面所說，Ubuntu 20.04所帶的node.js版本過低，我們需要更新node.js版本。我們選擇使用NVM來管理它。

第一步：安裝`nvm`（[Node Version Manager](https://github.com/nvm-sh/nvm)）：

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```

第二步：加載`nvm`到你的shell中。你可以重啓shell或者執行下面的命令：

```
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

第三步：安裝一個合適版本的node.js（本文使用`16.13.2`）

```
nvm install 16.13.2
```

#### 安裝CSS

第一步：從npm倉庫下載CSS：

```
npm install @solid/community-server
```

該命令將所有依賴和軟件安裝到你當前的目錄下（`node_modules`子目錄）。取決於你的喜好，你也許會更喜歡加上`-g`參數來進行全局安裝。

第二步：啓動CSS來驗證它是否正常工作：

```
npx community-solid-server
```

該命令會（從`node_modules`裏）啓動CSS，並服務在`http://localhost:3000`。建議訪問該URL來驗證它是否正常工作——你可以選擇直接在服務器上訪問，使用SSH隧道，或暴露服務器的3000端口然後從本地訪問。如果使用SSH隧道，你可以執行下面的命令，然後在本地訪問`http://localhost:3000`：

```
ssh -L 3000:localhost:3000 DOMAIN.NAME
```

注意，在瀏覽器上打開該URL後，會展示初始化頁面。該步驟暫時不需要進行初始化，因爲默認配置是將數據保存在內存中，所以你進行的初始化選項會在關閉服務器後丟失。

### 設置Web服務器（及SSL證書）

本部分沒有什麼特殊的，就是正常的配置nginx服務器以及（爲HTTPS）獲取SSL證書。如果你有經驗，完全可以不參考本文的做法。但假如沒有經驗，本部分會讓你明白每一步都在做什麼，也許可以幫助你觸類旁通。

#### 安裝nginx

```
apt-get update && apt-get install -nginx
```

記得回答`y`。

#### 允許nginx服務的端口通過防火牆

```
ufw allow 'Nginx Full'
```

該步驟使用了Ubuntu特有的`ufw`命令來配置防火牆。

#### （可選）驗證nginx是否正常工作

你可能會希望先檢查一下nginx是否在正常工作，以便在出問題時更容易排查。

最簡單的方法是修改`/etc/nginx/sites-available/default`中的服務器配置，並更新`server_name`的值爲你的域名：

```
server_name DOMAIN.NAME;
```

然後重啓nginx：

```
systemctl restart nginx
```

訪問`http://DOMAIN.NAME`，看看你是否可以看到nginx默認的歡迎頁面。

#### 獲取HTTPS所需的SSL證書
本文使用[Let's Encrypt](https://letsencrypt.org/)來獲取證書。它的命令行程序名稱叫做`certbot`，我們首先要安裝它：

```
apt-get install certbot
```

然後，我們通過它獲取證書。比如下面的命令會爲`DOMAIN.NAME`和`*.DOMAIN.NAME`獲取證書，並使用DNS考察（challenge）來驗證你的確擁有該域名：

```
certbot -d DOMAIN.NAME -d *.DOMAIN.NAME --manual --preferred-challenges dns certonly
```

執行命令過程中，你會看到要做什麼的提示：

1. 同意條款，以及提供一些基本信息。
2. 設置DNS TXT（文本）記錄以通過DNS考察。你需要逐次添加兩條記錄——兩條需要同時存在（DNS允許這種行爲），不要用第二個覆蓋第一個。

成功執行後，你會被分配對應證書。它們會位於`/etc/letsencrypt/DOMAIN.NAME/`。

如果你以前沒有用過Let's Encrypt的話，需要注意一下證書的有效期。Let's Encrypt證書是免費的，自始至終都是這樣（除非它們放棄了）。但它分配的證書只有90天的有效期，到期後需要我們自行重新獲取。`certbot`有對應的命令以方便地重新獲取，也可以設置計劃任務（比如使用cron腳本）自動進行。網上對這些東西有很多說明，我就不再贅述了。

#### 配置nginx使用HTTPS

現在需要配置nginx使用前面獲取到的證書，以使得服務器可以接受HTTPS連接。這裏使用Let's Encrypt提供的工具來自動修改nginx配置。

使用該工具需要先安裝一個依賴：

```
apt-get install python3-certbot-nginx
```

然後執行下面的命令來自動更新nginx配置：

```
certbot --nginx -d DOMAIN.NAME -d *.DOMAIN.NAME
```

過程中注意按提示走。它會修改你在其間所指定的nginx配置。

如果你是全新安裝且跟隨了前面的教程，這裏最好重啓nginx，並可以檢查HTTPS是否正常工作。

```
systemctl restart nginx
```

### 讓Web服務器和CSS配合工作

想讓Web服務器和CSS配合工作，需要分別配置兩邊：

1. 在Web服務器部分，設置反向代理至CSS（`http://localhost:3000`）；
2. 設置CSS接受來自代理的連接。

下面介紹如何在前述配置（nginx）下達成該工作。

#### 設置反向代理

CSS官網給了[如何在nginx設置反向代理的示例](https://solidproject.org/self-hosting/css/nginx)。其內容比較充實，下面的內容正是基於它。但這裏不會全盤重寫，有的地方仍然需要參照原文（主要是配置文件內容）。

這裏假設你的服務器配置文件是`/etc/nginx/sites-available/default`。

CSS官網示例中的修改內容，核心是這兩部分：

- `upstream`
- `location / {`

**首先**將`upstream`部分複製到你的服務器配置文件中，放在`server{}`部分之前即可（放在開頭也一樣）。

**第二步**是在`server{}`塊之內找到`location /`這一部分。注意這裏說的`server{}`塊是指使用`443`端口（HTTPS默認端口）的那部分配置。然後將CSS官網示例中`location /{}`部分的內容複製到你的配置文件中，放在剛剛找到的部分。原來的`location /{}`部分應當是有內容的，替換掉它們即可。

這其實就已經完成了。示例中提到的`/etc/nginx/snippets/https.conf`文件似乎並不重要，因爲它的絕大多數內容都已經在`certbot`自動配置的時候寫好了（但寫在`certbot`自己的文件下）。雖然實際上仍然有一小部分不在此列，但似乎並不影響。

現在可以訪問`https://DOMAIN.NAME`來查看效果了，應當可以看到Solid的網頁。

當然了，此時你應該會同時看到錯誤信息。這是正常現象，因爲我們還需要執行下面這一部分。

#### 修改CSS配置

前面的錯誤是因爲CSS不接受所配置的反向代理傳來的連接，因爲它認爲域名不匹配。我們需要在啓動CSS時同時告訴它域名應當是誰。這需要通過`-b`（或者`--basrUrl`）參數實現：`-b https://DOMAIN.NAME/`。

同時，你也很可能會希望可以持久化你的數據，也就是將它們存在硬盤上而非內存中。要達成這個效果，你需要指定一個合適的配置文件，以及選擇你要存儲在的目錄。例如，你可以使用`-c @css:config/file.json -f ~/Documents/`來告訴CSS載入它預製的`config/file.json`配置，並將文件（服務器設置和用戶數據）存在`~/Documents/`目錄下。實際上[CSS預製了許多配置](https://github.com/CommunitySolidServer/CommunitySolidServer/tree/main/config)，擁有很強大的定製性。不過這些都是進階使用和配置的部分了，本文就不詳述了。

有了這些準備，我們可以停下/打斷之前的CSS服務，然後使用下面的參數來再次啓動它：

```bash
npx community-solid-server -b https://DOMAIN.NAME/ -c @css:config/file.json -f ~/Documents/
```

啓動完畢後，從瀏覽器訪問`https://DOMAIN.NAME/`，你應當看到正常的CSS初始化界面。這時候可以跟隨它完成剩下的部分。這樣，你的CSS就初始化好了，你也可以使用它了。

如果以後對用戶數據或配置不滿，或者是由於任何原因數據損壞，可以考慮刪掉`~/Documents/`並重新初始化（不過希望你不會遇到這一天）。

### 配置不同的默認應用/界面

跟隨上面的步驟，我們得到了一個正常運行的Solid服務器。但你應當也發現了，通過網頁暫時沒什麼可做的，功能十分有限。這是因爲CSS出於模塊化的目的，只配備了最基礎的功能——一個Solid服務器（以及初始化及註冊賬號）。但既然已經是一個Solid服務器了，那麼你就可以使用任意的[Solid Apps（應用）](https://solidproject.org/apps)來管理和使用它了。

當然，你很可能會希望像 https://solidcommunity.net 等（使用NSS的）網站那樣，給你的Solid服務器提供一個基本的交互界面（如它們所用的[mashlib](https://github.com/solid/mashlib)），或者說設置一個默認應用。

CSS開發者們也想到這點了，並且爲此製作了一些*配方*，放在[CSS配方倉庫](https://github.com/CommunitySolidServer/Recipes)中。該倉庫也包含了對應的指導，以協助你進行基本配置/使用。你可以跟隨它們的說明，然後在啓動服務器時調整參數，以和前面所用參數匹配。

例如使用mashlib的話，你可以使用下面的命令以啓動（mashlib配方的）CSS：

```bash
npx community-solid-server -b https://DOMAIN.NAME/ -c config-mashlib.json -f ~/Documents/
```

啓動完畢後，訪問`https://DOMAIN.NAME/`應當會看到mashlib界面。這時候的體驗應當就和 https://solidcommunity.net 很接近了。

注意，目前設計中，如果你在初始化完成後還想註冊新用戶，需要在「登錄」界面點擊「註冊」，而不是直接在右上角點「註冊一個Solid賬號」（它會將你帶到Solid官網的服務器列表頁面）。當然，你也可以直接訪問對應的URL：`https://DOMAIN.NAME/idp/register/`。注意其最後的斜線不能漏。

## 結語

本文介紹了如何自行搭建一個Solid Community Server（CSS）。它是Solid社區新近開發並打算切換爲主力的服務器實現，最主要的優點是模塊化和可定製性。由於它的模塊化和定製性，它默認只有很簡單的界面，所以本文也簡單介紹了如何使用*配方*來提供默認應用。

總的來說，CSS的搭建並不複雜，主要是正常的Web服務器及反向代理配置；但CSS有自己的特點和選項，需要額外進行說明。

不過說到底，CSS只是一個Solid服務器。之所以自己搭建，是爲了將數據更安全的存放，或是提供更方便的Solid服務。但如在[《Solid——簡介與體驗》](solid-introduction)中介紹的那樣，使用Solid的核心是Solid App（應用），搭建了服務器後仍然需要使用恰當的App。從這個角度看，服務器在哪並沒有那麼重要。