Yoga 14s 2021上Arch Linux安裝細節及軟件（上）
##############################################

:date: 2022-07-03 18:21
:slug: archlinux-and-software-on-yoga-14s-2021-part-1
:lang: zh
:category: 信息技術
:tags: laptop, linux, 硬件, UEFI, rEFInd, snapper, BTRFS, 快照

.. include:: ./_common_include.rst
.. include:: ./_arch_include.rst

之前\ `寫了篇文章 <lenovo-yoga-14s-2021>`__\ 簡單談了談Yoga 14s 2021款的硬件及Linux兼容性。更早（多年）之前也有一些文章介紹\ `Linux裝機 <http://renyuneyun.is-programmer.com/posts/30254.html>`__\ 等相關話題。由於現在的新機器有了不少新特性，但相關說明似乎並不多見，故而借此文介紹我的機器相關的信息，以供有需要的人參考。

本文分上下兩部分：上部分主要講系統安裝維護相關的內容，下部分主要講新近功能及當前軟件配置相關的內容（含美化）。

.. |BOOT| replace:: :path:`/boot`
.. |ARCH-ROOT| replace:: :path:`/@root-arch`

系統安裝
===========

我上次寫Arch Linux的安裝還是在\ `2011年的文章 <http://renyuneyun.is-programmer.com/posts/30254.html>`__\ 中，當時還在用Acer 5745G，使用Grub (2)引導。而後換用XPS 13 (9343)後，便來到了UEFI和SSD，但並沒有更新說明——因爲當時arch的wiki中的安裝指導很完善，所以在之前的文章上貼了個告示了事。

時至今日，\ `arch wiki的安裝指導`_\ 仍然是最主要的參考對象。但其中一些地方是「可以這樣，可以那樣」，並要跳轉到對應頁面，對不瞭解的人並不友好——這樣那樣的選項導致的不同結果不夠明確。

當然了，wiki頁面的大部分內容還是挺順暢的，其主要問題存在於硬盤分區和引導器的選擇上。這裏以我的選擇及原因爲例，給出一個參考。

硬盤分區
----------

	順手一感慨：我打字的時候兩次順手打成「磁盤」。但現在已經是SSD了，已經不再用磁了……

我的機器預裝有Windows，而且我希望保留雙系統，所以需要Linux和Windows共存。

首先當然是在Windows中使用自帶的磁盤管理，從系統盤切下大部分爲Linux所用。我給Windows留下了200G，其他的全部劃歸Linux使用。

由於使用UEFI引導，Windows會在系統盤之前分配一些額外分區。其中第一個是ESP，給UEFI使用的，是UEFI標準的一部分；另外兩個我沒有去花費時間弄明白。在Linux中我們仍然需要給ESP寫入一些文件。

然後就是給Linux設計分區。這裏的確是按各人需求可以有各種不同的設計方法。我的需求如下：

- 用戶數據需要加密，系統不需要加密
- 系統會進行日常備份（使用snapper，下文會寫）
- 可能會多裝幾個發行版，但暫時僅是arch
- 我有16G內存，可能會進行休眠（掛起到硬盤）
- 空間不嫌多，可以透明壓縮就透明壓縮

於是，對於我來說，很自然的選擇就是系統分區使用BTRFS，並把系統的根作爲子卷。而由於BTRFS不支持對子卷進行加密，故而用戶數據放在單獨的用戶分區中並對分區進行加密（使用LUKS，即通過\ :cmd:`cryptsetup`\ 命令）。

UEFI最好的地方之一就是引導器裝在ESP中，且引導器可以提供對複雜文件系統的支持。這樣就不需要像以前BIOS時代一樣得把\ |BOOT|\ 單獨劃出。這樣\ |BOOT|\ 留在系統分區中，不需要額外設置或考慮，且備份可以自動進行。

最終結果上，我的Linux相關分區長成這樣：

.. code::

	├─nvme0n1p5 259:5    0    16G  0 part  [SWAP]
	├─nvme0n1p6 259:6    0    90G  0 part  /
	└─nvme0n1p7 259:7    0   169G  0 part  
	  └─home    254:0    0   169G  0 crypt /home

其中nvme0n1p5是我的SWAP分區；nvme0n1p6是我的系統分區；nvme0n1p7是加密的用戶分區，它解密後就是那個home。

出於多發行版及備份需求，我在系統分區中創建了一個子卷，叫做\ |ARCH-ROOT|\ ，用來存放我的archlinux根分區。掛載時需要額外設定\ |ARCH-ROOT|\ 。

另外，掛載時額外設置\ :code:`compress=zstd:3`\ 來進行透明壓縮——使用zstd的3級壓縮，一般被認爲是壓縮率和速度之間的較好平衡。

	一開始我是給\ |BOOT|\ 單獨劃了一個分區出來的。但後來隨着使用了snapper，那個分區變得不太合適，最終決定不再給\ |BOOT|\ 單獨分區。

引導器的選擇
------------

UEFI出現並普及的一大原因就是解決了BIOS時代引導的複雜及難以維護。但同時，出於各種因素，UEFI時代引導的選擇也多了起來，某種意義上更加複雜了。當然，隨着時間的推移，應當可以converge到有限的幾個選項中，只是現在還沒完全到那個時候。

UEFI時代不再需要寫\ :abbr:`MBR (Master Boot Record)`\ ，而是在UEFI固件（的設置空間）中添加引導項。這裏的引導項也不受MBR空間限制，而是需要作爲文件放在ESP中。

主要的引導方式有兩種：

- 直接通過UEFI進行引導
- 通過UEFI加載引導器，然後從引導器加載系統內核（類似BIOS時代的做法）

直接通過UEFI進行引導需要被引導的內核支持（即需要EFIStub，對arch來說不需要額外配置，因爲\ `默認已經啓用 <https://wiki.archlinux.org/title/EFISTUB>`__\ ），且需要爲每一個內核（及每一個內核引導配置）增加一個UEFI引導項。我只在XPS 13時試過一次，後來嫌麻煩就再也沒用過了。

這次也是一樣，我仍然選擇通過UEFI加載引導器，然後再加載內核。BIOS時代的Grub 2支持UEFI，但它的界面有些過時，而且明明用了framebuffer但仍然是字符界面。於是我選擇了rEFInd這個專門爲UEFI設計，且有圖形界面的引導器。

rEFInd
_________

.. |REFIND-CONF| replace:: :path:`$ESP/EFI/refind/refind.conf`
.. |REFIND-LINUX| replace:: :path:`refind_linux.conf`

安裝說明參照\ `wiki上的rEFInd頁面`_\ 即可。主要是需要將引導器安裝在ESP中（注意並非\ |BOOT|\ )，然後調整啓動項的配置。

對於Linux來說，rEFInd的啓動項配置主要是兩部分：

1. 寫在程序配置文件\ |REFIND-CONF|\ 中的靜態引導項
2. 從寫在某個\ *可識別*\ 處（如\ |BOOT|\ 下）的\ |REFIND-LINUX|\ 文件的內容動態創建的引導項

這裏的\ *可識別*\ 包含rEFInd預置的搜索路徑，如相關分區/文件系統根目錄下，或一些典型目錄。對於Linux，rEFInd會自動根據實際存在的內核動態生成引導項（arch不用調整內核文件命名結構，而其他發行版或許需要編輯\ |REFIND-CONF|\ ）。

我最初只寫了第二種，即在\ |REFIND-LINUX|\ 文件內聲明諸多引導項：

.. code::

	"Boot with standard options"  "i915.enable_psr=0 i8042.dumbkbd root=/dev/nvme0n1p6 rootflags=subvol=@root-arch splash quiet"
	"Boot with testing options (full)"  "i915.enable_psr=0 intel_idle.max_cstate=1 i8042.dumbkbd root=/dev/nvme0n1p6 rootflags=subvol=@root-arch  initrd=\intel-ucode.img initrd=\initramfs-%v.img"
	"Boot to single-user mode"    "i915.enable_psr=0 i8042.dumbkbd root=/dev/nvme0n1p6 rootflags=subvol=@root-arch single"
	"Boot with minimal options"   "i915.enable_psr=0 i8042.dumbkbd ro root=/dev/nvme0n1p6"

這麼做主要是爲了方便調試，尤其是剛拿到這臺電腦後的那段時間中（參見\ `《聯想Yoga 14s 2021款裝機小記》 <lenovo-yoga-14s-2021>`_\ ）。這樣做還有一個前提，那就是我的\ |BOOT|\ 最初放在單獨的分區中。但如果不是這樣（比如我後來廢除這個分區後），且仍然想要使用動態的引導項，則需要編輯\ |REFIND-CONF|\ 以告訴rEFInd去哪裏（對我來說就是\ :path:`@root-arch/boot`\ ）找動態的引導條目配置：

.. code::

	also_scan_dirs @root-arch/boot

一些關注細節的人可能看到，我的這幾個引導參數不完全一樣，尤其是第一項standard options中加了\ :code:`quiet splash`\ ，而其他的沒有。這兩個參數是爲了顯示\ `plymouth <https://www.freedesktop.org/wiki/Software/Plymouth/>`_\ 提供的splash，即加載動畫；而其他條目沒有則是因爲它們更多用於調試，所以我更希望顯示完整的加載報告。由於plymouth不是功能性的東西，所以放在下篇中講。

但在用了許久之後，我的引導參數趨於穩定。而後在使用了snapper之後，便換爲了使用\ |REFIND-CONF|\ 。這些見後文snapper相關章節。

使用snapper進行自動備份（快照）
===============================

前文提到我的根文件系統類型是BTRFS，這麼做的一個目的便是可以使用快照，並配合snapper進行自動備份。有了自動備份，我便可以隨時回滾到某個版本。這在某個更新後不好用時尤其有用。

	之前還提到，我的根目錄是作爲一個BTRFS子卷存在的。這個模式非常適合配合snapper進行自動備份。也有人玩一些更靈活的配置（參考\ `這裏 <https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout>`__\ ），但我沒有進行。一是沒時間，二是看起來會帶來別的副作用。

對snapper的設置其實不複雜，參照\ `wiki上的snapper頁面`_\ 即可。主要是要在其配置文件中設置好要備份的對象及備份選項（如何時備份），然後啓用對應的守護單元（主要是\ :code:`snapper-timeline.timer`\ 和\ :code:`snapper-cleanup.timer`\ ）。

.. tip::
	我的配置是保留2份月備份，2份週備份，2份日備份。

注意，snapper本身是openSUSE上所用的，並非arch所開發的。所以pacman默認是不會使用snapper在更新時進行備份的。爲了讓它支持，我安裝了\ :pkg:`snap-pac`\ 這個包——這個包提供了對應的pacman鉤子，會在更新前後各自動創建一個快照。

.. tip::
	當然了，由於機制所致，更新前後的快照中pacman數據庫鎖仍然處於加鎖狀態。對於我來說，這不是個問題，反而可以提醒我這是在快照中。

這樣，我的快照就會自動創建在\ :path:`/.snapshots/`\ 目錄之下。使用snapper對應命令可以列出並操作它們。

然而這樣還不夠，我還需要能夠自動將其加入引導項，以便有需要的時候直接在引導處進行切換。爲了達成這個目的，我使用了\ :pkg:`refind-btrfs`\ 。

使用\ :pkg:`refind-btrfs`\ 自動爲快照創建引導項
------------------------------------------------

如前文所述，我使用rEFInd作爲引導器，它可以支持兩種模式的引導項配置。\ :pkg:`refind-btrfs`\ 就是使用了第一種模式，並會自動維護快照的引導項。

使用\ :pkg:`refind-btrfs`\ 主要需要三步：

1. 創建示例引導項
2. 調整配置
3. 創建初次配置

第一步，我們需要在\ |REFIND-CONF|\ 中創建一個引導條目。該條目需要是一個可用的引導條目，尤其需要包含正確的子卷的路徑。我的條目如下：

.. code::

	menuentry "Arch Linux - Stable" {
	    icon /EFI/refind/icons/os_arch.png
	    volume "Linux Systems"
	    loader /@root-arch/boot/vmlinuz-linux-lily
	    initrd /@root-arch/boot/initramfs-linux-lily.img
	    options "i915.enable_psr=0 i8042.dumbkbd root=/dev/nvme0n1p6 rootflags=subvol=@root-arch"
	    submenuentry "Boot - fallback" {
	        initrd /@root-arch/boot/initramfs-linux-lily-fallback.img
	    }
	    submenuentry "Boot - terminal" {
	        add_options "systemd.unit=multi-user.target"
	    }
	}

條目中每一句話的詳細含義請自行對應文檔，或憑名稱猜測。其中重點的是兩部分：

1. 告訴rEFInd要去哪個分區處理啓動項，即\ :code:`volume "Linux Systems"`\ 的任務
2. 寫明帶子卷的啓動項，即\ :code:`loader`\ :code:`initrd`\ 和\ :code:`options`\ 中所有包含\ :code:`@root-arch`\ 的部分

.. note::

	我不知道refind-btrfs的作者是否強制要求子卷名稱以\ :code:`@`\ 開頭（上游示例中均包含），但這是許多人都遵守的命名方法，我也是其中之一。

第二步，編輯\ :path:`/etc/refind-btrfs.conf`\ 文件，以符合自己的情況。其中主要是這三節中的目錄：

1. :code:`[[snapshot-search]]`
2. :code:`[snapshot-manipulation]`
3. :code:`[boot-stanza-generation]`

默認配置就是基於配合snapper考慮而設置的，所以對於大多數人來說保留默認即可。我額外調整了\ :code:`[snapshot-manipulation]`\ 中的\ :code:`selection_count`\ 來添加更多條目。

第三步就是執行\ :cmd:`refind-btrfs`\ 來初始化並添加相應引導項。這裏主要是要注意是否有報錯，並解決對應錯誤。如果一切無誤，那麼它應當會自動在\ |REFIND-CONF|\ 的末尾添加一項\ :code:`include`\ ，並在對應文件中添加所要求個數的對應快照的引導項。

在完成這個基本配置並確定可用之後，如果希望自動維護啓動項，可以考慮啓用\ :code:`refind-btrfs.service`\ 這個服務，以自動監視對應目錄的變更。

其他雜項
===========

相比早期的安裝流程，當下的流程沒有結構性區別，但個別細節上有不同。其中最主要的就是不再使用\ :cmd:`wifi-menu`\ 連接WiFi，而是使用\ :cmd:`iwctl`\ ——比wifi-menu功能強大，但沒有它易用。

引導器之外，我依然關閉了Secure Boot，因爲屢次嘗試加上簽名都失敗了，而且似乎也沒有辦法給rEFInd加？我知道如紅帽和Canonical（Ubuntu的東家）等發行版廠商和微軟購買或談下了簽名，而且Arch的ISO似乎是可以在Secure Boot開啓的情況下正常引導的。但我始終沒找到合適的方案。最接近的是\ `這篇講rEFInd如何在Secure Boot下啓動的文章 <https://www.rodsbooks.com/refind/secureboot.html>`__\ ，但讀起來仍然過於複雜了。

一些眼尖的人應當看到了，我用的內核叫linux-lily，而不是默認的linux。這個內核是\ |archlinuxcn|\ 的百合仙子所維護的（當然，也放在\ |archlinuxcn repo|\ 中），對我來說主要優勢在於添加了tty下顯示中文的支持，並可以運行Anbox（因其支持ashmem和binder）。

.. note::
	Anbox所需要的這些內核模塊（尤其是ashmem）不再能夠在5.15（不含）以後的內核上使用DKMS的方式編譯，因爲允許這麼做會存在潛在風險（參考\ `這裏的說明 <https://wiki.archlinux.org/title/Anbox#Installing_anbox-modules-dkms>`__\ ）。所以如果想使用Anbox，就需要隨着內核編譯模塊。

最後補一下關於多發行版：前面提到，我分區和文件系統組織的一個目的是可以安裝其他發行版以和arch共存，以便萬一有意可以輕鬆遷移。我之前在XPS 13上其實做了和gentoo共存，但由於編譯以及對portage的不熟悉導致的麻煩，最終讓我沒換到gentoo去。後來又看到nixos，但僅在虛擬機中進行了簡單的嘗試，沒有實裝。這回做了準備，但由於各種因素，暫時還沒有嘗試去再裝一個發行版。所以這部分就留待以後再寫了。

.. _arch wiki的安裝指導: https://wiki.archlinux.org/title/Installation_guide
.. _wiki上的rEFInd頁面: <https://wiki.archlinux.org/title/REFInd>
.. _wiki上的snapper頁面: https://wiki.archlinux.org/title/Snapper