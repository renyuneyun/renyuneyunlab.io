再看開源協議——由貿易戰想到的
###############################

:date: 2019-05-23 12:17
:slug: reread-of-open-source-licenses-due-to-trade-war
:lang: zh
:category: tech
:tags: FLOSS, license

開源協議的選擇其實是一個老生常談的話題，其主要集中在GPL、LGPL、Apache 2、BSD和MIT協議上。雖然有時候也可能會加上MPL或者WTFPL，但絲毫不影響討論的主旨：商業。

在網上隨便一搜「開源協議對比」之類的文句，返回的結果一般都是在討論「哪個協議對商業更友好」之類的東西；結果基本都是或明或暗地在說：「避免GPL」，「選擇MIT、BSD、Apache」。

這一結論在其所限定的框架內的確沒錯，但處處透着不對勁。而我實在是太過習慣於發現不對勁後嘗試思考（雖然並不總是有結論）。於是，對這裏的不對勁，稍微思考便發現——和許多其他透着不對勁的話題一樣——它的問題出在前提上：開源的目的就是爲了商業使用？

   畢竟這是個資本主義的時代，商業主導了我們的生活，同時也規約人們的思維模式。商人們不斷宣傳「成功人士」以及金錢的神話，將創新和商業進行綁定，從而導致人們不自覺地認爲商業就是對的，「允許商業使用」是必須的前提。
   在上世紀末與本世紀初的時代，這一切都還可以被經濟增長所掩蓋。但到了這兩年，到了\ :abbr:`川建國同志 (Trump)`\ 撕下美國一直以來的面具和面子，進行保守主義轉向，並且對個別國家（比如中國）進行經濟「制裁」以至於科技封鎖（即貿易戰）開始，這一切也都再也無法掩蓋了。

在此條件下，此前（現在也是）基本無人在意的「（不）可剝奪性」便成了一個問題。

   如果由於自己所用的開源庫授權被撤銷了，導致自己的整個產品無法運行，這問題絕非小事。對於商業公司可能還好，畢竟他們本來也不對開源感興趣，開源對他們來說只是免費的午餐（所以他們纔會爲MIT等協議搖旗吶喊），沒了就沒了；但對於個人開發者或小社區來說，這點有可能是致命的，畢竟這些團體的產品很大程度上得益於開源軟件（並且這些人也更有意願回報開源社區）。誰也說不好川普會不會腦子一抽要求美國所有軟件產品（含源代碼）不能給中國用（啥？你跟我說他就不擔心後果？川普哪次做事的時候像是考慮過後果的樣子？），以及該策略會不會在未來被他人再次撿起。

邏輯上，該方式可以通過撤銷使用授權來完成：由於開源許可證/協議本身是著作權聲明（對開源許可證來說最重要的部分是使用授權），而著作權人有權撤銷該授權，所以這就是潛在的問題。
爲了避免該「漏洞」被以此種方式利用，我們便需要有意識地查看許可證中是否有「\ :abbr:`不可撤銷 (irrevocable)`\ 」的說法，而其合理的表述可以是「無論任何條件下（在滿足協議本身其他要求的基礎上）該授權都不可撤銷」。（另一個比較次要的，則是是否明言「\ :abbr:`全球的 (worldwide)`\ 」。）

我重新查看了一下上面提到的主要的許可證，很有趣，其是否有此聲明和此前所說的「普遍的結論」的傾向度正好相反：GPL、Apache均明確聲明了不可撤銷，而MIT和BSD（無論是2-Clause、3-Clause還是4-Clause）均沒有聲明。

當然，該結論並不令我太過意外。畢竟FSF和Apache Foundation本身就是開放的支持方，而MIT、BSD方面則更偏向於「無所謂」。要說哪裏超出預期，那便是我本以爲MIT和BSD中至少應該有一個有此聲明。

   需要注意的是，「沒有該聲明」不等於「一定會有此漏洞」。其原因之一是我並非法學相關專業的（希望相關專業人士前來指教），另一原因是或許此內容是隱含的只是我沒讀出來。

基於這個結論，我又去看了一下一些軟件的協議，其結果基本還是比較令我放心的。目下目光聚焦的Android，其使用Apache協議授權，意味着並無此風險（蛤？谷歌服務框架？谷歌是啥？）；而我更在意的GNU/Linux全系列的基礎全部都是GPL/LGPL協議，所以實際上更沒有問題。

   不少人都喜歡在討論時夾帶私貨，尤其喜歡在危言聳聽後趁人們不夠理智時夾帶。其中一個很令人討厭的地方就是喜歡強調「國產操作系統」，而他們口中的這個「國產操作系統」很明顯是要閉源的。

所以……嗯……整體而言還是比較光明的。但多少也給諸位多多少少有點國際主義意識、真心希望走向開放的人提了個醒：儘量不要選擇MIT、BSD一類的協議，以免哪天受困於這種莫名其妙的政治爭端。

