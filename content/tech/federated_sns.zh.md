Title: 互聯社交網絡
Date: 2018-8-24 1:49
Lang: zh
Category: 信息技術
Tags: 互聯, 社交網絡, 互聯網
Slug: federated-sns

互聯社交網絡是除了互聯即時通信系統/IM以外的另一主要互聯系統類型。從來沒有接觸過這類系統的人可以類比互聯的效果爲：直接在新浪微博關注騰訊微博的人（而不需要去騰訊微博重新註冊賬號）。

> 之前在跟人介紹時，我發現有人會將互聯系統和SSO的效果搞混。事實上，兩者完全不同。仍然以上面在新浪微博S關注騰訊微博T爲例：SSO的本質仍然是在T上註冊了一個新賬戶，然後使用T上的賬戶去關注T上的另一個賬戶，而你的S賬戶上仍然是沒有對T上的關注；但互聯SNS的效果是你直接在S上關注T上的人，不需要在T上重新生成一個賬戶。如果仍然不理解，那請思考在你不登錄T時，即使你做過了“關注”，在你的S賬戶能否看到T上被你關注的人的消息。

幾大比較知名的互聯SNS系統爲：

* [Mastodon](https://joinmastodon.org/)
* [GNU Social](https://gnu.io/social/)
* [Pump.io](http://pump.io/)
* [Diaspora\*](https://diasporafoundation.org/)
* [Friendica](https://friendi.ca/)
* [HubZilla](https://project.hubzilla.org/)

既然叫互聯系統，每個互聯SNS之內各個不同實例/服務器間的用戶可以互操作。事實上，這些系統主要分爲兩種協議（以及HubZilla的Zot），每個協議內不同系統也可互操作。

下文將以協議爲主要線索串聯不同互聯SNS系統特點與分別、大概歷史、以及我個人對其看法。其中內容來自我對不同互聯SNS系統的瞭解（包括使用），以及背景閱讀（尤其是下文中將會經常引用其圖片的[這篇文章](https://cdn-images-1.medium.com/max/1600/1*3pEK-Fwq7bNOVcnXfVdNuQ.png)）。

## The Federation與The Fediverse

<img src="{static}/images/federated_sns_1.png" width="600" />
（來自https://medium.com/we-distribute/a-quick-guide-to-the-free-network-c069309f334）

一般來說，在OStatus協議（及其衍生協議）下的系統（組成的網絡）被統稱爲The Fediverse；Diaspora協議下的系統（組成的網絡）被稱爲The Federation。

> Federation一詞就是“聯邦”之義；Fediverse應該是Federation和Diverse（“多樣的”）兩個詞的組合。

但事實上，如圖中所示，不像商業網絡，The Fediverse和The Federation並非涇渭分明——許多系統同時支持這兩者。比如上面提到但沒有分類的Friendica就同時支持兩個網絡；HubZilla在支持自己的Zot網絡之外，也同時支持這兩者。

之所以最初有協議的分別，除了（不知道有沒有）純粹的感情因素以外，系統的目的是影響協議設計的主要區別。其中，OStatus協議設計爲微博客使用；Diaspora協議被設計來服務更偏Facebook的展板/貼子型社交網絡。於是，The Federation中系統的UI設計脫則胎於Twitter；The Fediverse中系統的UI設計則更偏向Facebook。

> OStatus協議這邊稍有更多歷史可講：OStatus協議是GNU Social整合了StatusNet之後爲StatusNet協議改名的產物；而後來，Pump.io在OStatus的基礎上進行了一定改動形成了自己的協議。在2012年冬天之後，OStatus主要被一個W3C工作組維護；而後來，W3C成立了W3C Federated Social Web Working Group，並基於Pump.io的協議制定了新的ActivityPub協議，用以取代OStatus（參考自這裏）。

## ActivityPub

2017年W3C將ActivityPub協議立爲推薦的標準，用來取代之前的OStatus協議。

<img src="{static}/images/federated_sns_2.png" width="600" />
（仍然來自https://medium.com/we-distribute/a-quick-guide-to-the-free-network-c069309f334）

多數網絡均轉向支持ActivityPub協議（且許多系統已經實現ActivityPub），但Diaspora\*似乎並沒有做此打算。Nextcloud這一本來並不是SNS的系統也將支持ActivityPub協議，作爲跨站/互聯消息的分發機制。

我個人是比較希望各個網絡可以整合在一個協議之下，以減少維護成本。但ActivityPub畢竟衍生自微博客協議，由於我沒有閱讀過其協議內容，所以對該協議對其他形式內容（比如版面/貼子）的支持存在疑慮。

## HubZilla與Zot

HubZilla是一個比較獨特的系統：它沒有選擇使用OStatus或Diaspora協議，而是自己提出了新的Zot協議。就我個人瞭解來看，Zot並不是僅僅另一個協議：Zot最爲標彰的特點是所謂nomadic identity，這一點在其他協議（包括ActivityPub）中並不存在。

簡單來說，nomadic identity允許兩件事：

* 使用站點A的身份去登錄站點B（同樣，不需要在B上創建賬戶，數據仍然存在A上）
* 當有需要時，將站點A的身份（包括內容）整個克隆至站點B，並且所有關注了A站點身份的人將自動轉向關注站點B的身份

這兩點特性十分符合我的心意：

* 遠程認證這點省去了每次遇到新網站均需要重新註冊賬號的麻煩
  * 多年來（大約四年），我一直在（時不時）尋找遠程登錄的機制，而找到的最佳機制是OpenID一類。然而，且不說我至今都沒有弄明白的OpenID版本，OpenID在多數網站上仍然被作爲一種SSO實現：之前提過，SSO本質上也是重新註冊。
* 身份的轉移則是更多考慮站點切換的問題
  * 由於允許身份轉移，vender locking的情況會大大降低，迫使服務提供方不能選擇壓榨用戶
  * 在特殊情況下（比如站點不再維護），身份的轉移降低了遷移成本

當然，第二條主要在沒有GDPR的時候（+的地區）比較重要——GDPR規定服務商必須允許用戶將自己的數據（用戶自己上傳/填寫的所有數據）導出成機器可讀的格式。但即使是有GDPR，用戶仍然需要想辦法手動告知其他用戶自己的遷移。

在[一篇文章](https://medium.com/we-distribute/the-do-everything-system-an-in-depth-review-of-hubzilla-3-0-692204177d4e)中，HubZilla直接被稱爲*全包平臺*（The Do-Everythign System），體現了HubZilla的壯志/野心：HubZilla希望（Zot協議）成爲基礎，並且在其上構建各種服務，包括SNS、論壇、數據存儲……

我個人（四五年來）一直比較希望論壇可以被設計爲互聯系統，但一直沒有找到。HubZilla的設計給了我這個希望，且其實現已經部分涵蓋該目的。

## 使用體會及總結

我個人最近嘗試過Mastodon、Diaspora\*、HubZilla，印象中以前也試過pump.io以及GNU Social但記憶不是很深刻。

首先，必須要強調的是，不同於舊式的中心化社交網絡，互聯社交網絡是去中心化的。去中心化帶來多種便利，但同時也可能會導致從舊時代過來的人思維遇到障礙：互聯社交網絡的“官方”只有項目網站，並沒有“官方實例”（雖然官方一般都提供實例使用，但這僅僅是諸多對等實例中的一個）。想要使用互聯社交網絡，請選擇任意一個你喜歡的實例，或是自己搭建一個實例。一般而言，官方或第三方會維護一份（到多份）實例清單（由於去中心化的本質，清單並不全面，但足夠使用），可以從中選擇一個。

基本而言，Mastodon符合其微博客服務的定位，且功能全面（內容、可見性、API及授權控制）；另外其多欄的UI算是比較有特點，打破多數微博客服務那種中部瀑布的模式。另外，Mastodon似乎沒有爲動態設置地理位置的選項。

Diaspora\*符合正常的以主題爲線索的社交網絡（如facebook）定位，但不包含遊戲等功能。當然，完全可以將Diaspora\*當作微博客使用。

HubZilla則是一個全功能的平臺，可以當微博客、長文型社交網絡、相冊、論壇……需要注意的是，HubZilla將每一*頻道*作爲一個主體（每個賬號可以建立多個channel，彼此默認獨立），並且以頻道作爲管理單位。我也試過其nomadic identity，遷移自己的頻道並且保持聯動的更新，其效果的確符合預期。 然而HubZilla的界面看起來總有一種奇怪的簡陋感，即使其每個元素並不給人這種感覺。或許自定義主題後會有所改進，但我僅僅嘗試了默認提供的數套主題。

在測試中，每種網絡內互相關注都很輕易；跨網絡的互通測試結果如下（並沒有測試所有可能的組合）：

* HubZilla和Mastodon
  * HubZilla可以輕易關注Mastodon上的人（以`SOME_ONE@MASTODON.INSTANCE`形式增加 連接 ）
  * Mastodon上可直接搜索HubZilla上的人（只要該HubZilla實例支持和Mastodon互聯，一般爲ActivityPub協議）
  * HubZilla上以`MASTODON.INSTANCE/SOME_ONE`形式關注Mastodon上的人似乎有問題，其頭像右下角會顯示“禁止”圖標
* HubZilla關注Daspora*上的人似乎需要對方的審覈（會在其頭像右下角顯示一個“禁止”圖標）

> 詳細使用體會見[部分互聯社交網絡測試感受](http://renyuneyun.gitlab.io/wikiblog/#%E9%83%A8%E5%88%86%E4%BA%92%E8%81%AF%E7%A4%BE%E4%BA%A4%E7%B6%B2%E7%B5%A1%E6%B8%AC%E8%A9%A6%E6%84%9F%E5%8F%97)

如前面所述，整體而言，我更喜歡HubZilla的設計，於是自己在某一HubZilla實例上安了個窩。（具體地址見個人介紹頁面，這裏不再贅述；如果找不到，歡迎用你所知的任何可以聯繫我的方式詢問。） 如果是爲非我自己的人推薦，Mastodon應該是我首推的，畢竟無論是功能還是設計上看我個人都很喜歡它。
