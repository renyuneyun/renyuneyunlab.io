Title: Solid——簡介與體驗
Date: 2022-03-11 21:26
Category: tech
Tags: Solid, Decentralization, Linked Data, Semantic Web
Slug: solid-introduction
Lang: zh

你也許在其他地方聽過[SoLiD（Social Linked Data）](https://solidproject.org/)，它是諸多對現有萬維網不滿的改進項目之一。Solid之所以特別，其中一個重要因素在於它是由萬維網的發明者Tim Berners-Lee提出的——因爲他不滿意於時下萬維網及因特網的強烈中心化現狀，希望從技術層面改變這一趨勢，並給用戶以更多控制權。

Solid的特別之處並不只是它的願景，還在於它的技術和它的生態結構。由於中文互聯網上對Solid特點、相關技術以及如何使用或搭建Solid這些話題資料較少，本文嘗試對它們進行介紹，以協助潛在感興趣的讀者瞭解Solid生態及更廣泛的[Linked Data（鏈接數據，簡稱LD）](https://zh.wikipedia.org/zh-cn/%E9%8D%B5%E9%80%A3%E8%B3%87%E6%96%99) / [語義網（絡）](https://zh.wikipedia.org/wiki/%E8%AF%AD%E4%B9%89%E7%BD%91)的生態或技術鏈。

由於Solid仍在發展，LD及語義網也是仍在推進的研究方向，本文不會也無法對它們進行「全面」的展開。但本文會儘量選取其中最重要的部分進行介紹，並簡單提及相關其他技術。感興趣的讀者可以自行搜索瞭解更多知識。也是由於仍在發展，Solid生態主要是英文。感興趣的讀者可以考慮貢獻翻譯——假如它們支持翻譯的話。

下面一節將會簡要但更加深入地對Solid的設計（及其相關技術選取）進行介紹；第二節會介紹如何使用Solid；第三節介紹如何自己搭建一個Solid實例（以Solid Community Server爲例）。

## Solid的相關設計

Solid的願景和它的提出者是很有意義的背景信息，但這不足以支撐Solid的獨特或優秀之處。在本節，我會簡單介紹Solid在技術和設計上的特點。

### Solid所基於的技術

Solid所基於的技術是它之所以不同於其他項目的一大重要之處：[Linked Data （LD）](https://zh.wikipedia.org/zh-cn/%E9%8D%B5%E9%80%A3%E8%B3%87%E6%96%99)。LD可以被認爲是[語義網（絡）](https://zh.wikipedia.org/wiki/%E8%AF%AD%E4%B9%89%E7%BD%91)的新名字，也可以被認爲是語義網相關技術的衍生產物。它的最主要用處是描述數據以及描述數據之間的關聯，也就是語義網的核心。它也沿用了語義網的相關技術，如[RDF （Resource Description Framework）](https://en.wikipedia.org/wiki/Resource_Description_Framework)、[OWL（Web Ontology Language）](https://en.wikipedia.org/wiki/Web_Ontology_Language)等。

> 在當下這個時代，語義網經常不被提起，但實際上它仍然廣泛存在。尤其是其相關技術，如RDF、OWL等，在計算機及計算機以外的各領域的使用在逐步增加。如果一定要對它定性，與其說這是最後的餘暉，不如說是黎明前的黑暗。

LD之所以重要，是因爲它在描述數據的同時（或其後）也描述了數據之間的關聯（正如其名稱所預示的那樣）。這樣，所有數據成了有機的互相關聯的整體，且這是機器可讀的形式，這大大方便了更廣泛的數據獲取、挖掘等事。

這裏多說一下：LD是一種理念，而它的具體表示則使用了RDF的一系列技術；而RDF也是一種概念，其具體表示可以使用多種序列化方案，且它們理論上均等價。最常見的序列化形式是三種：RDF/XML、Turtle和JSON-LD。如名稱所示，RDF/XML（`.xml`或`.rdf`）是一種使用XML來序列化RDF的方案，而JSON-LD（`.jsonld`或`.json`）是使用JSON來序列化RDF或者說LD的方案。Turtle（`.ttl`）是一種廣爲使用，精煉且人類可讀性更好的序列化形式，也是Solid中最常用的數據存儲形式。

### Solid生態及Solid App（應用）

Solid的生態結構也是它的一個特點。它完全顛覆了現下流行的萬維網或因特網生態結構。

現下流行的萬維網及因特網生態結構中，數據和計算/服務均是由同一廠商/平臺提供的，用戶手中的僅有客戶端。各個互聯網公司的軟件（尤其是手機軟件）都是如此。這樣，該廠商/平臺擁有對數據的生殺大權，經常以此來脅迫用戶；而用戶對此無法有效反抗——唯一手段是放棄自己現有的所有數據，換一個廠商/平臺。這也是各個公司都在搞「雲端數據存儲」的原因。一些法律法規試圖改進該現狀，如GDPR要求所有廠商/平臺必須允許用戶將自己在該平臺上的所有數據導出爲機器可讀格式，但它們只是對此打補丁，並不能從源頭解決問題。

在Solid中，Solid服務器僅存儲用戶數據（及數據相關信息，如訪問權限），並提供數據的訪問接口；Solid App（應用）提供計算、用戶界面等業務。這樣，用戶只要擁有自己的Solid服務器，就可以輕易「轉換」（而不是「遷移」）計算業務提供方（也就是App）。當然，用戶也可以選擇使用公開的可信的Solid服務器。

於是，一個用戶的Solid賬號對應了他的Solid pod，也就是數據存儲（後文均稱爲pod）。這個pod存儲了用戶的所有數據，以及他的ID信息（在Solid中就是WebID）。而Solid App需要從用戶的pod中存取數據。對用戶來說，最直觀的感受有兩點：

1. 只需要一個Solid賬號（或者說pod，或者說WebID）就可以了，不需要爲每一個App註冊一個賬號。

2. Solid的使用就是圍繞App展開的：想要什麼功能，就尋找並打開對應的App，在其中登錄上自己的Solid賬號。

官方網站有一個[App列表](https://solidproject.org/apps)。我們也可以自己去其他地方尋找Solid App。

在下一節，我會圍繞Solid平臺體驗進行介紹。其中一個有意思的地方在於：這些Solid服務的默認網頁界面其本質也是一個Solid App（該App有時候也被稱爲SolidOS），其背後本質上是一個叫做Mashlib的軟件/庫。

## Solid平臺簡單體驗

對於不知道自己是否會喜歡Solid的人，可以先找一個公開的Solid平臺，在其上註冊一個賬號體驗一下。

[Solid官方網站的這裏](https://solidproject.org/users/get-a-pod)提供了一些開放的Solid服務平臺，以供選用。該頁面也提到了可以自己搭建，感興趣的讀者可以參考下一篇博文。

> 本文假設讀者在 https://solidcommunity.net 註冊了賬號，且用戶名爲`YOUR-NAME`。

註冊賬號的過程很普通，這裏就不再贅述。在之後體驗Solid App時，你需要登錄你的賬號——所以記住你的pod的提供商/網站是必要的。

註冊完賬號之後，可以先查看你的WebID，即`https://YOUR-NAME.solidcommunity.net/profile/card#me`。從瀏覽器中直接訪問這個URL（網址）會打開一個展示頁面，其中顯示了該文檔中的主要信息。

![Solid WebID profile page]({static}/images/solid/profile-page.png)

上圖是我在創建一個新用戶後訪問該頁面後打開的效果。可以看到其中每一欄都是空的，因爲新賬戶內沒有填寫任何信息。但可以看到，其中包含你的名稱、頭像、好友列表、個人簡介等內容。鼠標光標移動至點擊右上角的個人頭像處，會出現新的菜單（如下圖所示），其中有一項即是修改自己的用戶資料。在那裏，可以看到多種可以填寫的信息。

![Solid menu]({static}/images/solid/menu.png)

也許你注意到了，URL中包含`#me`部分。如果你刪掉這部分，那麼打開的頁面會是另一個樣子——打開一個RDF文件。如果你有一些RDF知識，那麼你應該意識到了，你的個人資料URL指向了你的pod中的一個文件（`/profile/card`）中的一個節點（`#me`）。這個`/profile/card`文件本質上是一個Turtle文件，感興趣的話可以點擊「代碼」按鈕（也即第四個按鈕）打開看一下它的內容。

在直接進入你的pod時，或者在菜單中點擊Your stuff後，你會看到一個較爲完整的操作界面。在第一個面板/標籤裏，你可以創建各類該實例所配置的資料——我之所用「資料」而非「文件」、「數據」等詞，是因爲它們可能簡單也可能複雜，但重點是它們代表的內容而非它們的存儲本質。實際上，本質上它們一般都是Turtle文件——RDF表示的豐富和擴展性使得这样使用十分自如。如下圖所示，其中有待辦事項、聯繫人、記事本、聊天等多種類別。

![Mashlib main page]({static}/images/solid/mashlib-main.png)

額外說一句，通過瀏覽器訪問的`https://YOUR-NAME.solidcommunity.net`網站時，打開的其實是一個集成的叫做[Mashlib](https://github.com/solid/mashlib)的Solid App。它也有獨立的版本，可以[從這裏](https://solid.github.io/mashlib/dist/browse.html)訪問。因爲它是Solid社區維護的一項核心入口，比較完善地集成了許多基礎性功能（如圖形界面/Web界面訪問和修改個人WebID檔案的內容，創建和修改其他資料），所以也經常被稱爲Solid OS。但如前文所說，它只是一個App，完全可以開發其他App來代替。

前面提到過，Solid官方網站有一個[App列表](https://solidproject.org/apps)，我們可以從中挑選一些App試用。這些App基本上都是運行在瀏覽器中的，所以可以直接打開使用——當然，需要連接到你的Solid pod上（當然也可以是別人的，只要你有對應的權限）。有的App可能沒有提供在線樣例，所以可能給出在本地執行的實例——一般都是三部分：1. 克隆倉庫；2. 安裝依賴；3. 啓動。

## Solid上的身份和跨實例交流

Solid既然秉持去中心化的理念，那麼它的設計自然是不會將用戶綁在某一個服務器也就是實例上的。這一理念在理解的人眼中理所當然，但在習慣了傳統的平臺的人那裏則或許是需要更多解釋的。

前面提到過，在創建了賬號後，你會擁有一個WebID，也就是那個URL。這個URL是你的身份標識，其（對應的文件）中描述了你的身份信息（以及一些額外的配置信息）。

在使用Solid App時，你往往需要使用你的WebID去登錄它們，否則你只能訪問「所有人」都可以訪問的內容。而Solid App會去讀取你的pod內的信息，來決定它們的行爲。

這樣，我們可以很容易地得到一個結論：我的WebID所在的服務器不重要，我的WebID的內容以及我的pod中存儲的信息纔重要。這樣，我們不會被控制在某個特定的服務器/平臺上，因爲任意服務器都是一樣的，只要它們可以按照標準規定的協議提供相應的數據——而標準和協議是公開的。這樣，我們可以隨意選擇一個自己信得過的服務器，或者是自己搭建服務器。

理論上說，我們可以任意遷移自己的WebID到其他服務器上。只要WebID內容一樣，且pod的數據也一樣，那麼App的表現也會一樣。但這裏還有一些額外的需要注意的部分：其他人（如你的好友）可能需要更新他們對你的身份的識別；App需要保證它們沒有在內部保存並使用你的pod數據而不是你的WebID URL來識別你。

但不考慮這些額外情況的時候，你可以任意切換你的身份提供方。額外地，Solid標準中支持[使用`owl:sameAs`來標識多個文檔](https://github.com/solid/solid-spec/blob/master/solid-webid-profiles.md#public-and-private-profiles)刻劃WebID身份，也有討論如何支持多個身份頁面，如[這裏](https://github.com/solid/solid-spec/issues/189)。但Solid暫時還沒有如同Hubzilla的nomadic identity一般容易使用的多服務器共享身份（參考我以前寫的[《互聯社交網絡》](federated-sns)一文）。

同樣地，使用WebID作爲ID之後，服務器本身不再重要，重要的只是WebID對應的URL作爲身份標識。於是在Solid中，「本服務器上的多個用戶」和「不同服務器上的多個用戶」沒有本質不同。Solid的所有機制對跨服務器和不跨服務器一視同仁。

總的來看，Solid中選擇服務器只需要考慮服務器的特性（容量、速度等），而不用考慮服務器會不會對你的使用造成影響。這比以前在[《互聯社交網絡》](federated-sns)中講到的各聯邦SNS服務提供了更大的自由度，因爲我們是和App交互而不是和pod（在服務器上）交互，而App是獨立於pod之外的。

## 結語

在本文中，我們介紹了Solid的基本特性，給出了體驗Solid的指導並涉及了可能會遇到的一些障礙，以及簡單討論了Solid去中心化的身份設計。

Solid的特點當然不止於此，但本文不打算也沒有辦法全部介紹，就好像你無法完全介紹現在的整個萬維網一樣。我希望前面的內容可以給你一些基本的介紹，讓你知道Solid的存在，以及越過最初的無所適從。如果你有興趣，完全可以嘗試更多的東西。

如果有時間，下一篇我會簡單介紹一下如何建立自己的Solid實例。我將會以[Solid Community Server](https://github.com/CommunitySolidServer/CommunitySolidServer)爲例——它是Solid社區最新近構建的Solid服務器實現，模塊化更好，並且擁有更好的擴展和定製性。
