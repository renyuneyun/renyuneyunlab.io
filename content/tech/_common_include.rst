.. include:: ../_common_include.rst

.. role:: path(code)
   :language: shell

.. role:: cmd(code)
   :language: shell

.. role:: shell(code)
   :language: shell

.. role:: rust(code)
   :language: rust

.. role:: py(code)
   :language: python

.. role:: java(code)
   :language: java
