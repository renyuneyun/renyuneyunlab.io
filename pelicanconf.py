#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'renyuneyun'
SITENAME = '覆沉'
SITEURL = ''

TIMEZONE = 'Europe/London'

PLUGIN_PATHS = ["plugin", "plugin/pelican-plugins"]
PLUGINS = [
        "assets",
        "category_meta",
        "extract_toc",
        "i18n_subsites",
        "post_stats",
        "related_posts",
        "series",
        "sitemap",
        "summary",
        "tipue_search",
        "interrefs",
        ]

PATH = 'content'

STATIC_PATHS = ['images', 'extras']

EXTRA_PATH_METADATA = {
        'extras/custom.css': {'path': 'static/custom.css'},
        'extras/favicon.ico': {'path': 'favicon.ico'},
}

IGNORE_FILES = ['_*']

# For tipue_search
DIRECT_TEMPLATES = ['index', 'tags', 'categories', 'authors', 'archives', 'search']

CUSTOM_CSS = 'static/custom.css'

THEME = './theme/simplify'

#TEMPLATE_PAGES = {'series.html':'series.html'}

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

DEFAULT_LANG = 'zh'
OG_LOCALE = 'zh_CN'
LOCALE = 'zh_HK.UTF-8'

DATE_FORMATS = {
    'zh': ((u'zh_HK', 'UTF-8'), u'%Y年%m月%d日(週%a)',),
    'zhs': ((u'zh_CN', 'UTF-8'), u'%Y年%m月%d日(周%a)',),
}

I18N_SUBSITES = {
    'zhs': dict(
        LOCALE='zh_CN.UTF-8',
        # SITENAME="「覆沉」",
        STATIC_PATHS=STATIC_PATHS
    ),
}

I18N_UNTRANSLATED_ARTICLES = "remove"
I18N_TEMPLATES_LANG = "en"  # 或許需要檢查主題要求


SITELOGO = SITEURL + '/images/profile_160.png'
SITETITLE = '「覆沉」'
SITESUBTITLE = '對沒錯，這是「人云E云」那貨的博客'

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-nc-sa'
}

COPYRIGHT_YEAR = '2019'

DISCLAIMER = 'Powered by <a href="https://github.com/getpelican/pelican" title="Pelican">Pelican</a>; <a href="https://kura.io/eevee/" title="Eevee">Eevee</a> theme by <a href="https://kura.io/" title="kura.io">kura.io</a>'
COPYRIGHT = '<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Unless otherwise stated, work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.'

THEME_PRIMARY = 'deep_orange'
THEME_ACCENT = 'blue'

PYGMENTS_STYLE = 'monokai'

USE_FOLDER_AS_CATEGORY = False

DEFAULT_PAGINATION = 8

USE_AUTHOR_CARD = True
AUTHOR_CARD_AVATAR = '/images/profile_160.png'
AUTHOR_CARD_DESCRIPTION = '''Arch Linux用戶；閒暇時爲FLOSS做做貢獻；認同自由軟件理念。<br />自認唯物論者；反對任意形式的迷信。<br />觀察者；在意社會問題；拒絕先入爲主。'''
AUTHOR_CARD_SOCIAL = (
        ('<i class="fa fa-globe aria-hidden="true"></i>',
            'https://hub.zilla.tech/channel/renyuneyun'),
        ('<i class="fa fa-comments aria-hidden="true"></i>',
            'https://matrix.to/#/@renyuneyun:matrix.org'),
        )

COMMENTS_ON_PAGES = True

MAIN_MENU = True

MENUITEMS = (
        ('Rust學習筆記', 'rust-note.html'),
        )

DISPLAY_PAGES_ON_MENU = True

# Blogroll
LINKS = (
        ('(っ・Д・)っ前博客', 'http://renyuneyun.is-programmer.com/'),
        )

# Social widget
# SOCIAL = (
#         ('<i class="fa fa-github aria-hidden="true"></i> GitHub', 'https://github.com/renyuneyun/'),
#         ('<i class="fa fa-gitlab aria-hidden="true"></i> GitLab', 'https://gitlab.com/renyuneyun/'),
#         ('<i class="fa fa-envelope aria-hidden="true"></i> Email', 'mailto:email@ryey.icu'),
#         )
SOCIAL = {
        'github': 'https://github.com/renyuneyun/',
        }

SOCIAL_WIDGET_NAME = 'Find me'


SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': .99,
        'pages': .75,
        'indexes': .5
    },
    'changefreqs': {
        'articles': 'weekly',
        'pages': 'weekly',
        'indexes': 'weekly'
    },
}

# Development settings

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
